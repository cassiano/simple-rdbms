// Sample execution:
//
//    deno run ./benchmarks/joins.ts --users=10 --tasks=50 --runs=50 --show-data
//
// If an error about lack of heap space is observed, use the appropriate Deno runtime flag:
//
//    deno run --v8-flags=--max-old-space-size=7000 ./benchmarks/joins.ts --users=1E4 --tasks=1E7 --runs=5 --no-show-data

import { parseArgs } from 'https://deno.land/std@0.207.0/cli/parse_args.ts'
import { createHumanity } from 'https://deno.land/x/humanity@1.5.0/mod.ts'

import { AvlTree } from '../avl_tree.ts'
import { elapsedTimesWithStats, filterMap, log, printObject, sample, timesMap } from '../util.ts'
import { openFilenames } from '../util.ts'

const PARTIAL_COUNTS_DISPLAY_SIZE = 1e6

const Humanity = createHumanity('en_US')

const flags = parseArgs(Deno.args, {
  string: ['users', 'tasks', 'filtered-users', 'filtered-tasks', 'runs'],
  boolean: ['show-data', 'show-trees'],
  default: {
    users: 1e5,
    tasks: 1e6,
    'filtered-users': undefined,
    'filtered-tasks': undefined,
    runs: 10,
    'show-data': false,
    'show-trees': false,
  },
  negatable: ['show-data', 'show-trees'],
})

if (flags['filtered-users'] !== undefined && +flags['filtered-users'] > +flags.users)
  throw new Error('Filtered users must be <= users')
if (flags['filtered-tasks'] !== undefined && +flags['filtered-tasks'] > +flags.tasks)
  throw new Error('Filtered tasks must be <= tasks')

// User has many tasks.
type UserType = {
  id: number
  name: string
}

// Task belongs to user.
type TaskType = {
  id: number
  title: string
  done: boolean
  userId: number
}

type JoinType = UserType & TaskType
type IndexType = AvlTree<number, number[]>

type UsersIndexType = {
  indexes: {
    primaryKey?: IndexType
  }
}

type TasksIndexType = {
  indexes: {
    userId?: IndexType
  }
}

log(`Generating ~ ${Humanity.number(+flags.users)} user(s)...`)

const users: UserType[] = timesMap(+flags.users, i => ({
  id: i + 1,
  name: `User-${i + 1}`,
}))

log(`Generating ~ ${Humanity.number(+flags.tasks)} task(s)...`)

const tasks: TaskType[] = timesMap(+flags.tasks, i => ({
  id: i + 1,
  title: `Task-${i + 1}`,
  done: sample([true, false]),
  userId: sample(users).id,
}))

const usersTableInfo: UsersIndexType = {
  indexes: {},
}

log("Filling users' PK index:")

users.forEach((user, rowNumber) => {
  if (rowNumber % 1e5 === 0) log(Humanity.number(rowNumber))

  if (!usersTableInfo.indexes.primaryKey) {
    usersTableInfo.indexes.primaryKey = new AvlTree<number, number[]>(user.id, [rowNumber])
    return
  }

  const node = usersTableInfo.indexes.primaryKey.find(user.id)

  if (node) node.value.push(rowNumber)
  else usersTableInfo.indexes.primaryKey.add(user.id, [rowNumber])
})

const tasksTableInfo: TasksIndexType = {
  indexes: {},
}

log("Filling tasks' FK index:")

// Speed up the construction of the FK index in `tasks` by rearranging them by `userId` in ascending order.
const tasksOrderedByUserId = tasks
  .map((task, rowNumber) => [rowNumber, task] as const)
  .sort((left, right) => left[1].userId - right[1].userId)

tasksOrderedByUserId.forEach(([rowNumber, task], i) => {
  if (i % 1e5 === 0) log(Humanity.number(i))

  if (!tasksTableInfo.indexes.userId) {
    tasksTableInfo.indexes.userId = new AvlTree<number, number[]>(task.userId, [rowNumber])
    return
  }

  const node = tasksTableInfo.indexes.userId.find(task.userId)

  if (node) node.value.push(rowNumber)
  else tasksTableInfo.indexes.userId.add(task.userId, [rowNumber])
})

log('pk count:', usersTableInfo.indexes.primaryKey?.count())
log('fk count:', tasksTableInfo.indexes.userId?.count())

if (flags['show-trees']) {
  const pkImage = await usersTableInfo.indexes.primaryKey?.toGraphViz('pk')
  const fkImage = await tasksTableInfo.indexes.userId?.toGraphViz('fk')

  if (pkImage && fkImage) openFilenames([pkImage, fkImage])
}

const filteredUsers = flags['filtered-users'] !== undefined ? users.slice(0, +flags['filtered-users']) : users
const filteredTasks = flags['filtered-tasks'] !== undefined ? tasks.slice(0, +flags['filtered-tasks']) : tasks

log(
  `Conceptually, the cartesian product between ${Humanity.number(
    Number(flags['filtered-users'] ?? flags.users)
  )} user(s) and ${Humanity.number(
    Number(flags['filtered-tasks'] ?? flags.tasks)
  )} task(s) would produce ~ ${Humanity.number(filteredUsers.length * filteredTasks.length)} row(s).`
)

if (flags['show-data']) printObject({ filteredUsers, usersTableInfo, filteredTasks, tasksTableInfo })

const partialCounts = { join1: 0, join2: 0 }

const joinResult1 = elapsedTimesWithStats<JoinType[]>(
  () => {
    partialCounts.join1 = 0

    return filteredUsers.flatMap(user => {
      if (++partialCounts.join1 % PARTIAL_COUNTS_DISPLAY_SIZE === 0)
        log('join1 count:', Humanity.number(partialCounts.join1))

      const userTasks = filterMap(tasksTableInfo.indexes.userId?.find(user.id)?.value ?? [], rowNumber =>
        filteredTasks.at(rowNumber)
      ) as TaskType[]

      return userTasks.map(task => ({ ...user, ...task }))
    })
  },
  {
    message: 'join 1: from the users end',
    repeat: +flags.runs,
    matchResults: false,
  }
)

joinResult1.result = joinResult1.result.sort((left, right) => left.id - right.id)

if (flags['show-data']) printObject({ joinResult1 })

const joinResult2 = elapsedTimesWithStats<JoinType[]>(
  () => {
    partialCounts.join2 = 0

    return filterMap(filteredTasks, task => {
      if (++partialCounts.join2 % PARTIAL_COUNTS_DISPLAY_SIZE === 0)
        log('join2 count:', Humanity.number(partialCounts.join2))

      const taskUser = filteredUsers.at(usersTableInfo.indexes.primaryKey?.find(task.userId)?.value?.[0]!)

      if (taskUser !== undefined) return { ...taskUser, ...task }
    }) as JoinType[]
  },
  {
    message: 'join 2: from the tasks end',
    repeat: +flags.runs,
    matchResults: false,
  }
)

joinResult2.result = joinResult2.result.sort((left, right) => left.id - right.id)

if (flags['show-data']) printObject({ joinResult2 })

// Check if joins return exactly the same rows and with the same length as tasks.
const joinsMatched =
  joinResult1.result.length === joinResult2.result.length &&
  // joinResult1.result.length === tasks.length &&
  joinResult1.result.every((row1, i) => {
    const row2 = joinResult2.result[i]

    return (
      row1.id === row2.id &&
      row1.name === row2.name &&
      row1.title === row2.title &&
      row1.done === row2.done &&
      row1.userId === row2.userId
    )
  })

if (!joinsMatched) throw new Error(`>>>>> Joins do not match <<<<<`)

if (joinResult1.stats.avgElapsedTime === joinResult2.stats.avgElapsedTime) log('\n===> Joins are equivalent')
else if (joinResult1.stats.avgElapsedTime < joinResult2.stats.avgElapsedTime)
  log(
    `\n===> 1st join is ${
      joinResult1.stats.avgElapsedTime > 0
        ? `${((1 - joinResult1.stats.avgElapsedTime / joinResult2.stats.avgElapsedTime) * 100).toFixed(1)}%`
        : 'much'
    } faster than 2nd one`
  )
else
  log(
    `\n===> 2nd join is ${
      joinResult2.stats.avgElapsedTime > 0
        ? `${((1 - joinResult2.stats.avgElapsedTime / joinResult1.stats.avgElapsedTime) * 100).toFixed(1)}%`
        : 'much'
    } faster than 1st one`
  )
