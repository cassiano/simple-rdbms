/// <reference lib="deno.ns" />

import { assertEquals } from 'https://deno.land/std@0.212.0/assert/mod.ts'
import { sqlStatement } from './sql_parsers.ts'

Deno.test('[DML] Select statement', () => {
  assertEquals(sqlStatement('SELECT * FROM tasks;'), [
    {
      type: 'Select',
      columns: '*',
      tables: [{ alias: undefined, name: 'tasks' }],
      where: undefined,
      orderBy: undefined,
    },
    '',
  ])
  assertEquals(sqlStatement('SELECT * FROM tasks1, tasks2, tasks3;'), [
    {
      type: 'Select',
      columns: '*',
      tables: [
        { alias: undefined, name: 'tasks1' },
        { alias: undefined, name: 'tasks2' },
        { alias: undefined, name: 'tasks3' },
      ],
      where: undefined,
      orderBy: undefined,
    },
    '',
  ])
  assertEquals(sqlStatement('SELECT id, title, done FROM tasks;'), [
    {
      type: 'Select',
      columns: [
        { alias: undefined, name: 'id' },
        { alias: undefined, name: 'title' },
        { alias: undefined, name: 'done' },
      ],
      tables: [{ alias: undefined, name: 'tasks' }],
      where: undefined,
      orderBy: undefined,
    },
    '',
  ])
  assertEquals(sqlStatement('SELECT id, title, done FROM tasks1, tasks2, tasks3;'), [
    {
      type: 'Select',
      columns: [
        { alias: undefined, name: 'id' },
        { alias: undefined, name: 'title' },
        { alias: undefined, name: 'done' },
      ],
      tables: [
        { alias: undefined, name: 'tasks1' },
        { alias: undefined, name: 'tasks2' },
        { alias: undefined, name: 'tasks3' },
      ],
      where: undefined,
      orderBy: undefined,
    },
    '',
  ])
  assertEquals(sqlStatement('SELECT id, title, done FROM tasks1 AS T1, tasks2 AS T2, tasks3 AS T3;'), [
    {
      type: 'Select',
      columns: [
        { alias: undefined, name: 'id' },
        { alias: undefined, name: 'title' },
        { alias: undefined, name: 'done' },
      ],
      tables: [
        { alias: 'T1', name: 'tasks1' },
        { alias: 'T2', name: 'tasks2' },
        { alias: 'T3', name: 'tasks3' },
      ],
      where: undefined,
      orderBy: undefined,
    },
    '',
  ])
  assertEquals(
    sqlStatement('SELECT T1.id, T2.title, T3.done FROM tasks1 AS T1, tasks2 AS T2, tasks3 AS T3;'),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: undefined,
        orderBy: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement('SELECT T1.id, T2.title, T3.done FROM tasks1 AS T1, tasks2 AS T2, tasks3 AS T3;'),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: undefined,
        orderBy: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      SELECT
        T1.id,
        T2.title,
        T3.done
      FROM
        tasks1 AS T1,
        tasks2 AS T2,
        tasks3 AS T3
      ORDER BY
        T1.id,
        2,
        T3.done;
    `),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: undefined,
        orderBy: [
          { column: { alias: 'T1', name: 'id' }, direction: undefined },
          { column: 2, direction: undefined },
          { column: { alias: 'T3', name: 'done' }, direction: undefined },
        ],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      SELECT
        T1.id,
        T2.title,
        T3.done
      FROM
        tasks1 AS T1,
        tasks2 AS T2,
        tasks3 AS T3
      WHERE
        T1.id >= 1 AND
        title <> 'A' AND
        T3.done = false
      ORDER BY
        T1.id,
        2,
        T3.done;
    `),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: [
          {
            left: {
              type: 'column',
              value: {
                alias: 'T1',
                name: 'id',
              },
            },
            operator: '>=',
            right: { type: 'scalar', value: 1 },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: undefined,
                name: 'title',
              },
            },
            operator: '<>',
            right: { type: 'scalar', value: 'A' },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: 'T3',
                name: 'done',
              },
            },
            operator: '=',
            right: { type: 'scalar', value: false },
          },
        ],
        orderBy: [
          { column: { alias: 'T1', name: 'id' }, direction: undefined },
          { column: 2, direction: undefined },
          { column: { alias: 'T3', name: 'done' }, direction: undefined },
        ],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      SELECT
        T1.id,
        T2.title,
        T3.done
      FROM
        tasks1 AS T1,
        tasks2 AS T2,
        tasks3 AS T3
      WHERE
        T1.id >= 1 AND
        title <> 'A' AND
        T3.done = false
      ORDER BY
        T1.id DESC,
        2 ASC,
        T3.done;
    `),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: [
          {
            left: {
              type: 'column',
              value: {
                alias: 'T1',
                name: 'id',
              },
            },
            operator: '>=',
            right: { type: 'scalar', value: 1 },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: undefined,
                name: 'title',
              },
            },
            operator: '<>',
            right: { type: 'scalar', value: 'A' },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: 'T3',
                name: 'done',
              },
            },
            operator: '=',
            right: { type: 'scalar', value: false },
          },
        ],
        orderBy: [
          { column: { alias: 'T1', name: 'id' }, direction: 'DESC' },
          { column: 2, direction: 'ASC' },
          { column: { alias: 'T3', name: 'done' }, direction: undefined },
        ],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      SELECT
        T1.id,
        T2.title,
        T3.done
      FROM
        tasks1 AS T1,
        tasks2 AS T2,
        tasks3 AS T3
      WHERE
        T1.id >= T2.id AND
        1 = T2.id AND
        2 < 3 AND
        title <> 'A' AND
        T3.done = true
      ORDER BY
        T1.id DESC,
        2 ASC,
        T3.done;
    `),
    [
      {
        type: 'Select',
        columns: [
          { alias: 'T1', name: 'id' },
          { alias: 'T2', name: 'title' },
          { alias: 'T3', name: 'done' },
        ],
        tables: [
          { alias: 'T1', name: 'tasks1' },
          { alias: 'T2', name: 'tasks2' },
          { alias: 'T3', name: 'tasks3' },
        ],
        where: [
          {
            left: {
              type: 'column',
              value: {
                alias: 'T1',
                name: 'id',
              },
            },
            operator: '>=',
            right: {
              type: 'column',
              value: {
                alias: 'T2',
                name: 'id',
              },
            },
          },
          {
            left: { type: 'scalar', value: 1 },
            operator: '=',
            right: {
              type: 'column',
              value: {
                alias: 'T2',
                name: 'id',
              },
            },
          },
          {
            left: { type: 'scalar', value: 2 },
            operator: '<',
            right: { type: 'scalar', value: 3 },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: undefined,
                name: 'title',
              },
            },
            operator: '<>',
            right: { type: 'scalar', value: 'A' },
          },
          {
            left: {
              type: 'column',
              value: {
                alias: 'T3',
                name: 'done',
              },
            },
            operator: '=',
            right: { type: 'scalar', value: true },
          },
        ],
        orderBy: [
          { column: { alias: 'T1', name: 'id' }, direction: 'DESC' },
          { column: 2, direction: 'ASC' },
          { column: { alias: 'T3', name: 'done' }, direction: undefined },
        ],
      },
      '',
    ]
  )
})

Deno.test('[DML] Insert statement', () => {
  assertEquals(
    sqlStatement(`
      INSERT INTO Customers (
        CustomerName, Active, Address, City, PostalCode, Country
      )
      VALUES
        ('Cardinal', true, 'Skagen 21', NULL, 4006, 'Norway'),
        ('Greasy Burger', false, 'Gateveien 15', 'Sandnes', 4306, 'Sweeden'),
        ('Tasty Tee', true, 'Streetroad 19B', 'Liverpool', 'L1 0AA', NULL);
    `),
    [
      {
        type: 'Insert',
        table: 'Customers',
        columns: ['CustomerName', 'Active', 'Address', 'City', 'PostalCode', 'Country'],
        rows: [
          ['Cardinal', true, 'Skagen 21', null, 4006, 'Norway'],
          ['Greasy Burger', false, 'Gateveien 15', 'Sandnes', 4306, 'Sweeden'],
          ['Tasty Tee', true, 'Streetroad 19B', 'Liverpool', 'L1 0AA', null],
        ],
      },
      '',
    ]
  )
})

Deno.test('[DDL] Create table statement', () => {
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id    INTEGER NOT NULL,
        title VARCHAR NULL,
        done  BOOLEAN
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: undefined,
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: undefined,
          },
        ],
        primaryKey: undefined,
        foreignKeys: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id    INTEGER NOT NULL,
        title VARCHAR NULL,
        done  BOOLEAN,
        PRIMARY KEY (id)
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: undefined,
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: undefined,
          },
        ],
        primaryKey: ['id'],
        foreignKeys: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id    INTEGER NOT NULL,
        title VARCHAR NULL,
        done  BOOLEAN,
        PRIMARY KEY (id, title, done)
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: undefined,
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: undefined,
          },
        ],
        primaryKey: ['id', 'title', 'done'],
        foreignKeys: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id    INTEGER NOT NULL,
        title VARCHAR     NULL DEFAULT '(no title)',
        done  BOOLEAN          DEFAULT false,
        PRIMARY KEY (id, title, done)
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: '(no title)',
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: false,
          },
        ],
        primaryKey: ['id', 'title', 'done'],
        foreignKeys: undefined,
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id      INTEGER NOT NULL,
        title   VARCHAR     NULL DEFAULT '(no title)',
        done    BOOLEAN          DEFAULT false,
        user_id INTEGER NOT NULL,
        PRIMARY KEY (id, title, done),
        FOREIGN KEY (user_id) REFERENCES users (id)
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: '(no title)',
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: false,
          },
          {
            offset: 3,
            name: 'user_id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
        ],
        primaryKey: ['id', 'title', 'done'],
        foreignKeys: [
          {
            source: { columns: ['user_id'] },
            target: { table: 'users', columns: ['id'] },
          },
        ],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE TABLE tasks (
        id        INTEGER NOT NULL,
        title     VARCHAR     NULL DEFAULT '(no title)',
        done      BOOLEAN          DEFAULT false,
        user_id   INTEGER NOT NULL,
        type_name VARCHAR,
        type_id   INTEGER,
        PRIMARY KEY (id, title, done),
        FOREIGN KEY (user_id) REFERENCES users (id),
        FOREIGN KEY (type_name, type_id) REFERENCES types (name, id)
      );
    `),
    [
      {
        type: 'CreateTable',
        table: 'tasks',
        columns: [
          {
            offset: 0,
            name: 'id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 1,
            name: 'title',
            type: 'VARCHAR',
            nullable: true,
            default: '(no title)',
          },
          {
            offset: 2,
            name: 'done',
            type: 'BOOLEAN',
            nullable: true,
            default: false,
          },
          {
            offset: 3,
            name: 'user_id',
            type: 'INTEGER',
            nullable: false,
            default: undefined,
          },
          {
            offset: 4,
            name: 'type_name',
            type: 'VARCHAR',
            nullable: true,
            default: undefined,
          },
          {
            offset: 5,
            name: 'type_id',
            type: 'INTEGER',
            nullable: true,
            default: undefined,
          },
        ],
        primaryKey: ['id', 'title', 'done'],
        foreignKeys: [
          {
            source: { columns: ['user_id'] },
            target: { table: 'users', columns: ['id'] },
          },
          {
            source: { columns: ['type_name', 'type_id'] },
            target: { table: 'types', columns: ['name', 'id'] },
          },
        ],
      },
      '',
    ]
  )
})

Deno.test('[DDL] Drop table statement', () => {
  assertEquals(
    sqlStatement(`
      DROP TABLE tasks;
    `),
    [
      {
        type: 'DropTable',
        table: 'tasks',
      },
      '',
    ]
  )
})

Deno.test('[DDL] Create index statement', () => {
  assertEquals(
    sqlStatement(`
      CREATE INDEX index1 ON tasks (title);
    `),
    [
      {
        type: 'CreateIndex',
        unique: false,
        name: 'index1',
        table: 'tasks',
        columns: ['title'],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE UNIQUE INDEX index1 ON tasks (title);
    `),
    [
      {
        type: 'CreateIndex',
        unique: true,
        name: 'index1',
        table: 'tasks',
        columns: ['title'],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE INDEX index1 ON tasks (id, title, done);
    `),
    [
      {
        type: 'CreateIndex',
        unique: false,
        name: 'index1',
        table: 'tasks',
        columns: ['id', 'title', 'done'],
      },
      '',
    ]
  )
  assertEquals(
    sqlStatement(`
      CREATE UNIQUE INDEX index1 ON tasks (id, title, done);
    `),
    [
      {
        type: 'CreateIndex',
        unique: true,
        name: 'index1',
        table: 'tasks',
        columns: ['id', 'title', 'done'],
      },
      '',
    ]
  )
})

Deno.test('[DDL] Drop index statement', () => {
  assertEquals(
    sqlStatement(`
      DROP INDEX tasks_title_idx;
    `),
    [
      {
        type: 'DropIndex',
        name: 'tasks_title_idx',
      },
      '',
    ]
  )
})
