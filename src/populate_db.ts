// https://medium.com/deno-the-complete-reference/v8-flags-supported-by-deno-f5f7a946dadb
// deno run --v8-flags=--stack-size=8192 ./populate_db.ts

// Check below how to stablish a simple Deno (REPL) interactive session:
//
// ✗ deno repl --v8-flags=--stack-size=8192,--max-old-space-size=8192 --allow-read --allow-net --allow-write --allow-run
// Deno 1.39.1
// exit using ctrl+d, ctrl+c, or close()
// REPL is running with all permissions allowed.
// To specify permissions, run `deno repl` with allow flags.
//
// > import { database, runQuery, printQuery } from './src/populate_db.ts'
// > import { printObject, log, printTable, tap } from './src/util.ts';
//
// > runQuery('CREATE TABLE t1 (id INTEGER, name VARCHAR, PRIMARY KEY (id))')
// {
//   statement: {
//     type: "CreateTable",
//     table: "t1",
//     columns: [
//       {
//         index: 0,
//         name: "id",
//         type: "INT",
//         nullable: true,
//         default: undefined
//       },
//       {
//         index: 1,
//         name: "name",
//         type: "VARCHAR",
//         nullable: true,
//         default: undefined
//       }
//     ],
//     pk: [ "id" ]
//   }
// }
//
// > runQuery('INSERT INTO t1 (id, name) VALUES (1, "a"), (2, "b")')
// {
//   statement: {
//     type: "Insert",
//     table: "t1",
//     columns: [ "id", "name" ],
//     rows: [ [ 1, "a" ], [ 2, "b" ] ]
//   }
// }
// undefined
//
// > runQuery('SELECT * FROM t1 ORDER BY name DESC')
// {
//   statement: {
//     type: "Select",
//     columns: "*",
//     tables: [ { alias: undefined, name: "t1" } ],
//     where: undefined,
//     orderBy: [
//       { column: { alias: undefined, name: "name" }, direction: "DESC" }
//     ]
//   }
// }
// tablesCartesianProduct() returning 2 rows.
// ┌───────┬───────┬─────────┐
// │ (idx) │ t1.id │ t1.name │
// ├───────┼───────┼─────────┤
// │     0 │     2 │ "b"     │
// │     1 │     1 │ "a"     │
// └───────┴───────┴─────────┘
//
// > printObject(database)
// {
//   tables: {
//     t1: {
//       metadata: {
//         name: "t1",
//         columns: [
//           {
//             index: 0,
//             name: "id",
//             type: "INT",
//             nullable: true,
//             default: undefined
//           },
//           {
//             index: 1,
//             name: "name",
//             type: "VARCHAR",
//             nullable: true,
//             default: undefined
//           }
//         ],
//         indexes: { pk: { name: "pk", unique: true, columns: [ "id" ] } }
//       },
//       rows: [ [ 1, "a" ], [ 2, "b" ] ],
//       indexes: { pk: { "[1]": [ 0 ], "[2]": [ 1 ] } }
//     }
//   }
// }

import { parseArgs } from 'https://deno.land/std@0.207.0/cli/parse_args.ts'
import { createHumanity } from 'https://deno.land/x/humanity@1.5.0/mod.ts'

import { InsertType } from './sql_parsers.ts'
import { DatabaseType, executeInsert, executeSqlStatements } from './sql_runtime.ts'
import { filterMap } from './util.ts'
import {
  debug,
  delimitAsString,
  elapsedTimes,
  formatRowsForSqlInsertion,
  leftFillNumber,
  log,
  map,
  openFilenames,
  printTable,
  rand,
  sample,
  tap,
  timesMap,
} from './util.ts'

const Humanity = createHumanity('en_US')

export const database: DatabaseType = { tables: {} }

export const runQuery = (query: string) =>
  tap(executeSqlStatements(database, query), queryResult => {
    log(query)

    debug({ queryResult })
  })

export const printQuery = (query: string) => {
  printTable(runQuery(query)!)
}

const DEFAULT_COUNTS = {
  roles: 3,
  users: 100,
  tasks: 5000,
} as const

if (import.meta.main) {
  console.log('Running main...')

  const flags = parseArgs(Deno.args, {
    string: Object.keys(DEFAULT_COUNTS),
    boolean: ['fast-task-inserts', 'show-trees'],
    default: { ...DEFAULT_COUNTS, 'fast-task-inserts': false, 'show-trees': false },
    negatable: 'fast-task-inserts',
  })

  log(`Generating ~ ${Humanity.number(+flags.roles)} role(s)...`)
  log(`Generating ~ ${Humanity.number(+flags.users)} user(s)...`)
  log(`Generating ~ ${Humanity.number(+flags.tasks)} task(s)...`)

  const fakeData = {
    roles: timesMap(+flags.roles, i => [
      i + 1,
      delimitAsString(
        `role-${leftFillNumber(i + 1, Math.floor(Math.log10(+flags.roles) + 1))}`
        // faker.name.jobTitle()
      ),
    ]),
    users: timesMap(+flags.users, i => [
      i + 1,
      delimitAsString(
        `user-${leftFillNumber(i + 1, Math.floor(Math.log10(+flags.users) + 1))}`
        // faker.name.findName()
      ),
      Math.random() < 0.5
        ? 'NULL'
        : delimitAsString(
            // faker.internet.userName()
            `username-${leftFillNumber(i + 1, Math.floor(Math.log10(+flags.users) + 1))}`
          ),
      rand(+flags.roles) + 1,
    ]),
    tasks: timesMap(+flags.tasks, i => [
      i + 1,
      `task-${leftFillNumber(i + 1, Math.floor(Math.log10(+flags.tasks) + 1))}`,
      // delimitAsString(
      //   sample(['Buy', 'Sell', 'Trade']) + ' ' + faker.commerce.productName()
      // ),
      sample([true, false]),
      sample([1, 2, 3, 4, 5]),
      rand(+flags.users) + 1,
    ]),
  } as const

  const tasksInsertStatement = `
    INSERT INTO tasks (
      id, title, done, priority, user_id
    )
    VALUES
      ${formatRowsForSqlInsertion(
        map(fakeData.tasks, task =>
          task.map(value => (typeof value !== 'string' ? value : delimitAsString(value)))
        )
      )};
  `

  runQuery(
    `
      CREATE TABLE roles (
        id   INTEGER NOT NULL,
        name VARCHAR NOT NULL,

        PRIMARY KEY (id)
      );

      CREATE UNIQUE INDEX roles_name_idx ON roles (name);

      CREATE TABLE users (
        id       INTEGER NOT NULL,
        name     VARCHAR NOT NULL,
        username VARCHAR     NULL,
        role_id  INTEGER NOT NULL,

        PRIMARY KEY (id),
        FOREIGN KEY (role_id) REFERENCES roles (id)
      );

      CREATE UNIQUE INDEX users_name_idx     ON users (name);
      CREATE UNIQUE INDEX users_username_idx ON users (username);
      CREATE        INDEX users_role_id_idx  ON users (role_id);

      CREATE TABLE tasks (
        id       INTEGER NOT NULL,
        title    VARCHAR NOT NULL,
        done     BOOLEAN NOT NULL DEFAULT false,
        user_id  INTEGER NOT NULL,
        priority INTEGER NOT NULL,

        PRIMARY KEY (id),
        FOREIGN KEY (user_id) REFERENCES users (id)
      );

      CREATE INDEX tasks_user_id_idx ON tasks (user_id);

      INSERT INTO roles (
        id, name
      )
      VALUES
        ${formatRowsForSqlInsertion(fakeData.roles)};

      INSERT INTO users (
        id, name, username, role_id
      )
      VALUES
        ${formatRowsForSqlInsertion(fakeData.users)};

      ${flags['fast-task-inserts'] ? '' : tasksInsertStatement}

      CREATE        INDEX tasks_done_idx                       ON tasks (done);
      CREATE        INDEX tasks_user_id_and_done_idx           ON tasks (user_id, done);
      CREATE UNIQUE INDEX tasks_user_id_and_title_and_done_idx ON tasks (user_id, title, done);
    `
  )

  if (flags['fast-task-inserts']) {
    const insertStatement: InsertType = {
      type: 'Insert',
      table: 'tasks',
      columns: 'id, title, done, priority, user_id'.split(', '),
      rows: fakeData.tasks,
    }

    log(tasksInsertStatement)

    elapsedTimes(() => executeInsert(database, insertStatement), { message: 'INSERT INTO tasks...' })
  }

  debug({ database })

  // printQuery(`
  //   SELECT
  //     *
  //   FROM
  //     roles
  //   WHERE
  //     id >= 2
  //   ORDER BY
  //     name DESC
  // `)

  // printQuery(`
  //   SELECT
  //     users.id, users.name, username, roles.id, roles.name
  //   FROM
  //     users, roles
  //   WHERE
  //     role_id = roles.id
  //   ORDER BY
  //     username ASC,
  //     users.name
  // `)

  // printQuery(`
  //   SELECT
  //     *
  //   FROM
  //     roles AS R1, roles AS R2, roles AS R3, roles AS R4, roles AS R5
  //   ORDER BY
  //     R1.id,
  //     R2.id,
  //     R3.id,
  //     R4.id,
  //     R5.id
  // `)

  printQuery(`
    SELECT
      R.id, U.id, U.name, username, R.name, tasks.id, title, done, priority
    FROM
      users AS U, roles AS R, tasks
    WHERE
      user_id = U.id AND
      R.id = role_id AND
      priority BETWEEN 4 AND 5 AND
      done = true AND
      user_id = 2
    ORDER BY
      priority,
      6 DESC
  `)

  // printQuery(`
  //   SELECT
  //     COUNT(*)
  //   FROM
  //     users AS U, roles AS R, tasks
  //   WHERE
  //     user_id = U.id AND
  //     R.id = role_id AND
  //     priority BETWEEN 4 AND 5 AND
  //     done = true AND
  //     user_id = 2
  // `)

  if (flags['show-trees']) {
    const images = (
      await Promise.all(
        Object.values(database.tables).map(
          async table =>
            await Promise.all(
              filterMap(Object.entries(table.indexes), ([name, index]) =>
                index?.toGraphViz(`${table.metadata.name} ⇨ ${name}`)
              )
            )
        )
      )
    ).flat()

    openFilenames(images)
  }
}
