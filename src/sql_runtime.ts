import Humanity from 'https://deno.land/x/humanity@1.5.0/src/Humanity.ts'

import { AvlTree } from './avl_tree.ts'
import { isError } from './parser_combinators.ts'
import {
  ASTERISK,
  coerse,
  ColumnMetadataType,
  ColumnOrderType,
  COUNT_ASTERISK,
  CreateIndexType,
  CreateTableType,
  DropIndexType,
  DropTableType,
  FkMetadataType,
  InsertType,
  NameWithOptionalAliasType,
  PkMetadataType,
  RowType,
  ScalarType,
  SelectType,
  SqlConditionalOperatorType,
  SqlConditionType,
  SqlDirectionType,
  SqlExpressionType,
  sqlStatements,
  SqlStatementType,
} from './sql_parsers.ts'
import {
  arrayDifference,
  compare,
  debug,
  elapsedTimes,
  filterMap,
  forEach,
  leftFillNumber,
  log,
  matrixIntersection,
  memoize,
  reduce,
  setDebugMode,
  tap,
  uniqueValues,
} from './util.ts'

const JOINS_FK_PK_TABLE_ROWS_RATIO_THRESHOLD = 1 / 10
const FLOATING_POINT_NUMBER_EXAMPLE = 1 / 3
const MAX_NUMERIC_DIGITS = Math.max(
  Number.MAX_SAFE_INTEGER.toString().length,
  FLOATING_POINT_NUMBER_EXAMPLE.toString().length
)

type IndexMetadataType = {
  name: string
  unique: boolean
  columns: string[]
}

type IndexDataType = AvlTree<ScalarType, number[]>

type IndexMetadataContainerType = {
  [name: string]: IndexMetadataType
}

type TableMetadataType = {
  name: string
  columns: ColumnMetadataType[]
  primaryKey?: PkMetadataType
  foreignKeys?: FkMetadataType[]
  indexes: IndexMetadataContainerType
}

type IndexDataContainerType = {
  [name: string]: IndexDataType | undefined
}

type SqlTable = RowType[]

type TableType = {
  metadata: TableMetadataType
  rows: SqlTable
  // Index values will be a plain object, with keys containing (a string representation of) column
  // values and values containing an array of row indexes. In a real RDBMS we would use a B-Tree
  // instead, due to performance reasons.
  indexes: IndexDataContainerType
}

export type DatabaseType = {
  tables: {
    [name: string]: TableType
  }
}

type JoinedTablesEntryType = [predominantTable: string, ...otherTables: string[]]
type JoinedRowsEntryItemType = [predominantRow: number, ...otherRows: number[]]
type JoinedRowsEntryType = {
  [predominantRow: number]: JoinedRowsEntryItemType
}

type JoinedTablesItemType = {
  tables: JoinedTablesEntryType
  rows: JoinedRowsEntryType
}
type JoinedTablesType = JoinedTablesItemType[]

type TablesRowsType = {
  [tableNameOrAlias: string]: number[]
}

export type ResultTableType = [string[], ...RowType[]]

export const getTableMetadataByName = (database: DatabaseType, table: string) =>
  database.tables[table].metadata

export const getTableMetadataByNameOrAlias = (
  database: DatabaseType,
  tables: NameWithOptionalAliasType[],
  nameOrAlias: string
) => getTableInfoByNameOrAlias(database, tables, nameOrAlias).metadata

export const getTableInfoByName = (database: DatabaseType, table: string) => database.tables[table]

export const getTableInfoByNameOrAlias = memoize(
  (database: DatabaseType, tables: NameWithOptionalAliasType[], nameOrAlias: string) => {
    const matchedTable = tables.find(table => [table.alias, table.name].includes(nameOrAlias))

    if (matchedTable === undefined) throw new Error(`Table with name or alias "${nameOrAlias}" not found.`)

    return getTableInfoByName(database, matchedTable.name)
  }
)

const getColumnMetadata = memoize((database: DatabaseType, table: string, columnName: string) => {
  const metadata = getTableMetadataByName(database, table).columns.find(column => column.name === columnName)

  if (metadata === undefined) throw new Error(`Column metadata not found for column "${columnName}"`)

  return metadata
})

export const executeSqlStatements = (database: DatabaseType, statementsAsString: string) => {
  const [parsedStatements, rest] = sqlStatements(statementsAsString)

  if (isError(parsedStatements)) throw parsedStatements

  if (rest.trim() !== '') throw new Error(`SQL statement could not be parsed after: '${rest}'`)

  let result: ResultTableType | undefined

  parsedStatements.forEach(parsedStatement => {
    result = executeParsedSqlStatement(database, parsedStatement)
  })

  // Return the result of the last executed statement.
  return result
}

const tablesCartesianProduct = (joinedTablesRows: JoinedTablesType): JoinedTablesItemType => {
  debug(
    () =>
      `[tablesCartesianProduct()] Generating a total of ${joinedTablesRows.reduce(
        (memo, item) => memo * Object.keys(item.rows).length,
        1
      )} row(s).`
  )

  return joinedTablesRows.reduce((memo: JoinedTablesItemType, { tables, rows }) => ({
    tables: memo.tables.concat(tables) as JoinedTablesEntryType,
    rows: Object.values(memo.rows).flatMap(row1 =>
      Object.values(rows).map(row2 => row1.concat(row2) as JoinedRowsEntryItemType)
    ),
  }))
}

const getColumnMetadataFromConditionOperand = (
  database: DatabaseType,
  statement: SelectType,
  operand: SqlExpressionType
) => {
  if (operand.type !== 'column') throw new Error(`Expected operand to be column, but was '${operand.type}'`)

  const tableNameOrAlias =
    operand.value.alias ?? getTableNameOrAliasForColumn(database, statement, operand.value.name)

  const tableMetadata = getTableMetadataByNameOrAlias(database, statement.tables, tableNameOrAlias)

  return {
    tableNameOrAlias,
    columnMetadata: getColumnMetadata(database, tableMetadata.name, operand.value.name),
  }
}

const applyJoinCondition = (
  database: DatabaseType,
  statement: SelectType,
  condition: SqlConditionType,
  partialJoins: JoinedTablesType
) => {
  const { left, right } = condition

  if (left.type !== 'column' || right.type !== 'column')
    throw new Error(`Expected both condition operands to be columns`)

  const operand = { left, right }

  const tableNameOrAlias = {
    left: left.value.alias ?? getTableNameOrAliasForColumn(database, statement, left.value.name),
    right: right.value.alias ?? getTableNameOrAliasForColumn(database, statement, right.value.name),
  }

  const tableInfo = {
    left: getTableInfoByNameOrAlias(database, statement.tables, tableNameOrAlias.left),
    right: getTableInfoByNameOrAlias(database, statement.tables, tableNameOrAlias.right),
  }

  const indexMetadata = {
    left: Object.values(tableInfo.left.metadata.indexes).find(
      index => index.columns.length === 1 && index.columns[0] === left.value.name
    ),
    right: Object.values(tableInfo.right.metadata.indexes).find(
      index => index.columns.length === 1 && index.columns[0] === right.value.name
    ),
  }

  let pk: ColumnSidePropType
  let fk: ColumnSidePropType

  // Check which operands contain the PK and FK.
  if (indexMetadata.left?.name === 'primaryKey' && indexMetadata.right?.name !== 'primaryKey') {
    ;[pk, fk] = ['left', 'right']
  } else if (indexMetadata.left?.name !== 'primaryKey' && indexMetadata.right?.name === 'primaryKey') {
    ;[pk, fk] = ['right', 'left']
  } else
    throw new Error(
      'One of the columns of a join condition must have a PK, while the other can optionally have a FK (but never another PK)'
    )

  const joinedRows = {
    pk: partialJoins.find(({ tables }) => tables.includes(tableNameOrAlias[pk])),
    fk: partialJoins.find(({ tables }) => tables.includes(tableNameOrAlias[fk])),
  }

  if (joinedRows.pk === undefined || joinedRows.fk === undefined)
    throw new Error(
      `Rows not found for either the PK table '${tableNameOrAlias[pk]}' or FK table '${tableNameOrAlias[fk]}'`
    )

  const tableOffset = {
    pk: joinedRows.pk.tables.indexOf(tableNameOrAlias[pk]),
    fk: joinedRows.fk.tables.indexOf(tableNameOrAlias[fk]),
  }

  const columnOffset = {
    pk: tableInfo[pk].metadata.columns.find(columnMetadata => columnMetadata.name === operand[pk].value.name)
      ?.offset,
    fk: tableInfo[fk].metadata.columns.find(columnMetadata => columnMetadata.name === operand[fk].value.name)
      ?.offset,
  }

  if (columnOffset.pk === undefined || columnOffset.fk === undefined)
    throw new Error(
      `Column not found for either the PK table ('${operand[pk].value.name}') or FK table ('${operand[fk].value.name}')`
    )

  const rows = { pk: joinedRows.pk.rows, fk: joinedRows.fk.rows }
  const fkEntries = Object.entries(rows.fk)
  const pkValues = Object.values(rows.pk)
  const fkIndexMetadata = indexMetadata[fk]
  const pkRows = rows.pk
  const fkRows = rows.fk
  const pkColumnOffset = columnOffset.pk
  const fkColumnOffset = columnOffset.fk

  const useAlternateJoinMethodFromFkEnd =
    fkIndexMetadata === undefined || // 1) A FK index is not present; or
    fkEntries.length / pkValues.length < JOINS_FK_PK_TABLE_ROWS_RATIO_THRESHOLD || // 2) Rows in FK table are far fewer than the ones in the PK table; or
    joinedRows.fk.tables[0] !== tableNameOrAlias[fk] // 3) The FK table is not the predominant table (i.e., 1st) in the joined rows entry tables.

  debug({ tableNameOrAlias, useAlternateJoinMethodFromFkEnd })

  const joinedRowsResult: JoinedRowsEntryType = !useAlternateJoinMethodFromFkEnd
    ? reduce(
        pkValues,
        (memo: JoinedRowsEntryType, pkRow) =>
          tap(memo, acc => {
            const pkRowNumber = pkRow[tableOffset.pk]
            const pkTableRow = tableInfo[pk].rows[pkRowNumber]
            const pkValue = pkTableRow[pkColumnOffset]
            const pkIndexKeyValue = generateIndexKey([pkValue])
            const indexData = tableInfo[fk].indexes[fkIndexMetadata.name]

            const fkRowNumbers: number[] | undefined = indexData?.find(pkIndexKeyValue)?.value

            // Did we find any rows in the FK table corresponding (i.e., related) to this PK? There might be none.
            if (fkRowNumbers === undefined) return

            for (let i = 0; i < fkRowNumbers.length; i++) {
              const fkRowNumber = fkRowNumbers[i]
              const relatedFkRow = fkRows[fkRowNumber]

              // Check if the related FK row has been previously filtered out.
              if (relatedFkRow !== undefined)
                // Add the new/related FK row to the left (start) of the list, since the FK table is always the predominant table in a join.
                acc[fkRowNumber] = relatedFkRow.concat(pkRow) as JoinedRowsEntryType[number]
            }
          }),
        {}
      )
    : reduce(
        fkEntries,
        (memo: JoinedRowsEntryType, [fkPredominantRowNumber, fkRow]) =>
          tap(memo, acc => {
            const fkRowNumber = fkRow[tableOffset.fk]
            const fkTableRow = tableInfo[fk].rows[fkRowNumber]
            const fkValue = fkTableRow[fkColumnOffset]

            if (fkValue === null) return // No join in this case, since the FK is NULL.

            const fkIndexKeyValue = generateIndexKey([fkValue])
            const indexData = tableInfo[pk].indexes.primaryKey

            const pkRowNumber = indexData?.find(fkIndexKeyValue)?.value?.[0] // Read only 1st array item, giving it's a PK.
            const relatedPkRow = pkRows[pkRowNumber!]

            // Check if the related PK row has been previously filtered out.
            if (relatedPkRow !== undefined)
              // Add the new/related PK row to the right (end) of the list, since the FK table is always the predominant table in a join.
              acc[+fkPredominantRowNumber] = fkRow.concat(relatedPkRow) as JoinedRowsEntryType[number]
          }),
        {}
      )

  const [pkEntryJoinedTables, fkEntryJoinedTables] = [pk, fk].map(side => {
    const tableIndex = partialJoins.findIndex(entry => entry.tables.includes(tableNameOrAlias[side]))
    const joinedTables = partialJoins[tableIndex].tables

    // Delete table entry.
    partialJoins.splice(tableIndex, 1)

    return joinedTables
  })

  partialJoins.push({
    tables: [...fkEntryJoinedTables, ...pkEntryJoinedTables],
    rows: joinedRowsResult,
  })
}

const createInitialJoinedTables = (tablesRows: TablesRowsType): JoinedTablesType =>
  Object.entries(tablesRows).map(([tableNameOrAlias, rows]) => ({
    tables: [tableNameOrAlias] as const,
    rows: reduce(
      rows,
      (memo: JoinedRowsEntryType, row: number | undefined, i: number) =>
        tap(memo, acc => {
          row ??= i

          acc[row] = [row]
        }),
      {}
    ),
  }))

type ColumnSidePropType = 'left' | 'right'

const extractColumnInfoFromCondition = (
  condition: SqlConditionType
): {
  column: NameWithOptionalAliasType
  value: ScalarType
  columnSideProp: ColumnSidePropType
} => {
  if (condition.left.type === 'column' && condition.right.type === 'scalar')
    return {
      column: condition.left.value,
      value: condition.right.value,
      columnSideProp: 'left',
    }
  else if (condition.right.type === 'column' && condition.left.type === 'scalar')
    return {
      column: condition.right.value,
      value: condition.left.value,
      columnSideProp: 'right',
    }
  else throw new Error(`[extractColumnInfoFromCondition] Unexpected condition (${JSON.stringify(condition)})`)
}

type IndexedConditionsType = {
  tableNameOrAlias: string
  indexes: {
    indexName: string
    conditions: SqlConditionType[]
  }[]
}[]

const selectIndexedConditions = (
  database: DatabaseType,
  statement: SelectType,
  conditions: SqlConditionType[]
): IndexedConditionsType => {
  const indexedConditions: IndexedConditionsType = []

  const potentiallyIndexable: {
    conditions: SqlConditionType[]
    columns: NameWithOptionalAliasType[]
    tableAliases: string[]
    tableIndexesMetadata: [string, IndexMetadataContainerType][]
  } = {
    conditions: [],
    columns: [],
    tableAliases: [],
    tableIndexesMetadata: [],
  }

  potentiallyIndexable.conditions = conditions.filter(
    condition =>
      condition.operator === '=' &&
      [condition.left, condition.right].filter(operand => operand.type === 'column').length === 1
  )

  potentiallyIndexable.columns = potentiallyIndexable.conditions.map(
    condition => extractColumnInfoFromCondition(condition).column
  )

  potentiallyIndexable.tableAliases = uniqueValues(
    potentiallyIndexable.columns.map(
      columnInfo => columnInfo.alias ?? getTableNameOrAliasForColumn(database, statement, columnInfo.name)
    )
  )

  potentiallyIndexable.tableIndexesMetadata = potentiallyIndexable.tableAliases.map(
    alias => [alias, getTableMetadataByNameOrAlias(database, statement.tables, alias).indexes] as const
  )

  potentiallyIndexable.tableIndexesMetadata.forEach(([tableNameOrAlias, indexMetadata]) => {
    Object.values(indexMetadata)
      .sort((left, right) => right.columns.length - left.columns.length) // Sort by # of columns, in descending order.
      .forEach(index => {
        const applicableConditions = filterMap(index.columns, column =>
          potentiallyIndexable.conditions.find(condition => {
            const columnInfo = extractColumnInfoFromCondition(condition).column
            const columnAlias =
              columnInfo.alias ?? getTableNameOrAliasForColumn(database, statement, columnInfo.name)

            return columnAlias === tableNameOrAlias && columnInfo.name === column
          })
        ) as SqlConditionType[]

        // An index is considered valid if *all* its columns are present in at least 1 (potentially indexable) condition.
        if (applicableConditions.length === index.columns.length) {
          let indexedCondition = indexedConditions.find(
            indexedCondition => indexedCondition.tableNameOrAlias === tableNameOrAlias
          )

          // Create array entry if non-existent.
          if (indexedCondition === undefined)
            indexedConditions.push((indexedCondition = { tableNameOrAlias, indexes: [] }))

          indexedCondition.indexes.push({
            indexName: index.name,
            conditions: applicableConditions,
          })

          // Remove above applicable conditions from the potentially indexable ones.
          potentiallyIndexable.conditions = potentiallyIndexable.conditions.filter(
            condition => !applicableConditions.includes(condition)
          )
        }
      })
  })

  return indexedConditions
}

const operandsMatch = (
  leftValue: ScalarType,
  rightValue: ScalarType,
  operator: SqlConditionalOperatorType
) => {
  const comparisonResult = compare(leftValue, rightValue)

  switch (operator) {
    case '<':
      return comparisonResult === -1
    case '<=':
      return comparisonResult <= 0
    case '=':
      return comparisonResult === 0
    case '<>':
      return comparisonResult !== 0
    case '>':
      return comparisonResult === 1
    case '>=':
      return comparisonResult >= 0
    default: {
      const exhaustiveCheck: never = operator
      throw new Error(`exhaustiveCheck: ${exhaustiveCheck}`)
    }
  }
}

const joinTablesAndApplyWhereConditions = (
  database: DatabaseType,
  statement: SelectType
): JoinedTablesItemType => {
  log(new Date(), 'Starting applying joins and WHERE conditions')

  const conditions = statement.where
  const tables = statement.tables
  const tablesRows: TablesRowsType = {}

  tables.forEach(table => {
    tablesRows[table.alias ?? table.name] = Array(database.tables[table.name].rows.length)
  })

  if (conditions !== undefined) {
    const indexedConditionsInfo = selectIndexedConditions(database, statement, conditions)
    const indexedConditions = indexedConditionsInfo.flatMap(conditionInfo =>
      conditionInfo.indexes.flatMap(index => index.conditions)
    )

    const tableScanConditions = arrayDifference(conditions, indexedConditions).filter(
      condition => [condition.left, condition.right].filter(operand => operand.type === 'column').length === 1
    )

    const joinConditions = conditions.filter(
      condition =>
        condition.operator === '=' &&
        [condition.left, condition.right].filter(operand => operand.type === 'column').length > 1
    )

    const remainingConditions = arrayDifference(conditions, joinConditions).filter(
      condition => [condition.left, condition.right].filter(operand => operand.type === 'column').length > 1
    )

    debug({
      indexedConditionsInfo,
      tableScanConditions,
      joinConditions,
      remainingConditions,
    })

    debug(() => '# of rows before step 1:')
    debug({
      tablesRows: Object.entries(tablesRows).map(([table, rows]) => [table, rows.length]),
    })

    debug(() => 'Rows before step 1:')
    debug({ tablesRows })

    log(new Date(), 'Starting step 1')

    // 1) Apply conditions individually per table, using indexes.
    indexedConditionsInfo.forEach(conditionInfo => {
      const tableInfo = getTableInfoByNameOrAlias(database, statement.tables, conditionInfo.tableNameOrAlias)

      const rowsMatrix: number[][] = conditionInfo.indexes.map(index => {
        debug(() => `Applying index '${index.indexName}' from table '${tableInfo.metadata.name}'`)

        const indexKey = generateIndexKey(
          index.conditions.map(condition => extractColumnInfoFromCondition(condition).value)
        )

        debug({ indexKey })

        return tableInfo.indexes[index.indexName]?.find(indexKey)?.value ?? []
      })

      tablesRows[conditionInfo.tableNameOrAlias] = matrixIntersection(rowsMatrix)
    })

    debug(() => '# of rows before step 2:')
    debug({
      tablesRows: Object.entries(tablesRows).map(([table, rows]) => [table, rows.length]),
    })

    debug(() => 'Rows before step 2:')
    debug({ tablesRows })

    log(new Date(), 'Starting step 2')

    // 2) Apply more conditions per table, but this time without indexes and doing a slower table scan.
    tableScanConditions.forEach(condition => {
      const columnInfo = extractColumnInfoFromCondition(condition)

      const { tableNameOrAlias, columnMetadata } = getColumnMetadataFromConditionOperand(
        database,
        statement,
        {
          type: 'column',
          value: columnInfo.column,
        }
      )

      const tableInfo = getTableInfoByNameOrAlias(database, statement.tables, tableNameOrAlias)

      tablesRows[tableNameOrAlias] = filterMap(
        tablesRows[tableNameOrAlias],
        (rowNumber: number | undefined, i) => {
          rowNumber ??= i

          const row = tableInfo.rows[rowNumber]

          const [leftValue, rightValue] =
            columnInfo.columnSideProp === 'left'
              ? [row[columnMetadata.offset], condition.right.value as ScalarType]
              : [condition.left.value as ScalarType, row[columnMetadata.offset]]

          if (operandsMatch(leftValue, rightValue, condition.operator)) return rowNumber
        }
      )
    })

    // debug(() => '# of rows before step 3:')
    // debug({
    //   tablesRows: Object.entries(tablesRows).map(([table, rows]) => [
    //     table,
    //     rows.length,
    //   ]),
    // })

    debug(() => 'Rows before step 3:')
    debug({ tablesRows })

    log(new Date(), 'Creating initial joined tables')

    const partialJoins: JoinedTablesType = createInitialJoinedTables(tablesRows)

    debug({ partialJoins })

    log(new Date(), 'Starting step 3')

    // 3) Execute joins.
    joinConditions.forEach((condition, i) => {
      debug(() => `>>> Applying condition ${i + 1}: ${JSON.stringify(condition)}`)

      applyJoinCondition(database, statement, condition, partialJoins)

      // debug({
      //   joinedTablesRows: partialJoins.map(({ tables, rows }) => ({
      //     tables,
      //     rowsCount: Object.keys(rows).length,
      //   })),
      // })
      debug({ partialJoins })
    })

    debug(() => 'joinedTablesRows after joins:')
    debug({ partialJoins })

    log(new Date(), 'Starting cartesian product')

    // Do a Cartesian Product on the remaining tables.
    const joinedTables: JoinedTablesItemType = tablesCartesianProduct(partialJoins)

    debug(() => `# of rows before step 4: ${Object.keys(joinedTables.rows).length}`)
    debug({ joinedTables })

    log(new Date(), 'Starting step 4')

    // 4) Apply remaining conditions directly in the joins result, using a much slower table scan in the joined tables.
    remainingConditions.forEach(condition => {
      const { left, right } = condition

      if (left.type !== 'column' || right.type !== 'column')
        throw new Error(`Expected both condition operands to be columns`)

      joinedTables.rows = filterMap(Object.values(joinedTables.rows), row => {
        const leftValue = getColumnValueForRow(database, statement, row, left.value, joinedTables.tables)
        const rightValue = getColumnValueForRow(database, statement, row, right.value, joinedTables.tables)

        if (operandsMatch(leftValue, rightValue, condition.operator)) return row
      })
    })

    log(new Date(), 'After all steps')

    debug(() => `Final # of rows: ${Object.keys(joinedTables.rows).length}`)
    debug({ joinedTables })

    return joinedTables
  }

  const joinedTablesRows: JoinedTablesType = createInitialJoinedTables(tablesRows)

  return tablesCartesianProduct(joinedTablesRows)
}

const calculateOrderColumnAndDirection = memoize(
  (
    database: DatabaseType,
    statement: SelectType,
    order: ColumnOrderType
  ): {
    column: NameWithOptionalAliasType
    direction: SqlDirectionType
  } => {
    let column: NameWithOptionalAliasType

    if (typeof order.column === 'number') {
      if (statement.columns === ASTERISK || statement.columns === COUNT_ASTERISK)
        throw new Error(
          `Ordering columns by index (${order.column}) not allowed when '${ASTERISK}' or '${COUNT_ASTERISK}' are used.`
        )
      else if (order.column > statement.columns.length)
        throw new Error(
          `Column index ${order.column} must be ≤ column count of the SELECT clause (which is ${statement.columns.length}).`
        )
      else
        column = {
          name: statement.columns[order.column - 1].name,
          alias: statement.columns[order.column - 1].alias,
        }
    } else column = { name: order.column.name, alias: order.column.alias }

    column.alias = column.alias ?? getTableNameOrAliasForColumn(database, statement, column.name)

    const direction = order.direction ?? 'ASC'

    debug(
      () =>
        `Ordering by '${column.alias}.${column.name}' in ${
          direction === 'ASC' ? 'ascending' : 'descending'
        } order`
    )

    return { column, direction }
  }
)

const getColumnValueForRow = (
  database: DatabaseType,
  statement: SelectType,
  row: number[],
  column: NameWithOptionalAliasType,
  tables: string[]
): ScalarType => {
  const tableNameOrAlias = column.alias ?? getTableNameOrAliasForColumn(database, statement, column.alias)

  const tableInfo = getTableInfoByNameOrAlias(database, statement.tables, tableNameOrAlias)

  const columnMetadata = getColumnMetadata(database, tableInfo.metadata.name, column.name)

  const tableOffset = tables.indexOf(tableNameOrAlias)

  return tableInfo.rows[row[tableOffset]][columnMetadata.offset]
}

const getRowValues = (
  database: DatabaseType,
  statement: SelectType,
  tableNamesOrAliases: string[],
  row: number[]
): RowType => {
  const tablesRows = tableNamesOrAliases.map((tableNameOrAlias, tableOffset) => {
    const tableInfo = getTableInfoByNameOrAlias(database, statement.tables, tableNameOrAlias)

    return tableInfo.rows[row[tableOffset]]
  })

  return tablesRows.reduce((memo, tablesRow) => memo.concat(tablesRow))
}

const sortRows = (
  database: DatabaseType,
  statement: SelectType,
  joinResult: JoinedTablesItemType
): JoinedTablesItemType => {
  const { orderBy } = statement

  if (orderBy === undefined || Object.keys(joinResult.rows).length <= 1) return joinResult

  const orderedJoinResult: JoinedTablesItemType = {
    tables: joinResult.tables,
    rows: {},
  }

  orderedJoinResult.rows = Object.values(joinResult.rows).sort((leftRow, rightRow) => {
    for (const order of orderBy) {
      const { column, direction } = calculateOrderColumnAndDirection(database, statement, order)

      const leftColumnValue = getColumnValueForRow(database, statement, leftRow, column, joinResult.tables)

      const rightColumnValue = getColumnValueForRow(database, statement, rightRow, column, joinResult.tables)

      let comparison: number

      if (typeof leftColumnValue === 'string' && typeof rightColumnValue === 'string') {
        comparison =
          direction === 'ASC'
            ? leftColumnValue.localeCompare(rightColumnValue)
            : rightColumnValue.localeCompare(leftColumnValue)
      } else if (leftColumnValue === null && rightColumnValue === null) {
        comparison = 0
      } else if (leftColumnValue === null) {
        comparison = direction === 'ASC' ? -1 : 1 // Assume a left NULL value is always smaller than a right non-NULL one.
      } else if (rightColumnValue === null) {
        comparison = direction === 'ASC' ? 1 : -1 // Assume a left non-NULL value is always greater than a right NULL one.
      } else {
        // For boolean values: false < true.
        comparison =
          direction === 'ASC'
            ? Number(leftColumnValue) - Number(rightColumnValue)
            : Number(rightColumnValue) - Number(leftColumnValue)
      }

      if (comparison !== 0) return comparison
    }

    return 0
  })

  return orderedJoinResult
}

export const getTableNameOrAliasForColumn = memoize(
  (database: DatabaseType, statement: SelectType, searchedColumnName: string) => {
    let matchedTableAliasOrName: string | undefined = undefined

    for (const table of statement.tables) {
      const tableMetadata = getTableMetadataByName(database, table.name)
      const matchedColumn = tableMetadata.columns.find(column => column.name === searchedColumnName)

      if (matchedColumn !== undefined) {
        // Was the column already found in another table?
        if (matchedTableAliasOrName !== undefined)
          throw new Error(
            `Ambiguous column '${searchedColumnName}'. Please use a table name or alias to disambiguate it (you may use 1 of the following 2 options: '${matchedTableAliasOrName}' or '${
              table.alias ?? table.name
            }', keeping in mind that others could also be available).`
          )

        matchedTableAliasOrName = table.alias ?? table.name
      }
    }

    if (matchedTableAliasOrName === undefined) throw new Error(`Column '${searchedColumnName}' not found.`)

    return matchedTableAliasOrName
  }
)

const buildColumnName = (columnName: string, columnAlias: string) => `${columnAlias}.${columnName}`

const selectOutputColumns = (
  database: DatabaseType,
  statement: SelectType,
  queryResult: ResultTableType
): ResultTableType => {
  if (statement.columns === ASTERISK) return queryResult

  if (statement.columns === COUNT_ASTERISK) return [[COUNT_ASTERISK], [queryResult.length - 1]]

  const updatedQueryResult: ResultTableType = [[]]
  const [columnNames, ...rows] = queryResult

  const columns = statement.columns.map(column => {
    const columnAlias = column.alias ?? getTableNameOrAliasForColumn(database, statement, column.name)

    return buildColumnName(column.name, columnAlias)
  })

  updatedQueryResult[0] = columns

  rows.forEach(row => updatedQueryResult.push(columns.map(column => row[columnNames.indexOf(column)])))

  return updatedQueryResult
}

const executeCreateTable = (database: DatabaseType, statement: CreateTableType) => {
  if (database.tables[statement.table] !== undefined)
    throw new Error(`Error creating table '${statement.table}' (table already exists)`)

  if (statement.foreignKeys !== undefined)
    statement.foreignKeys.forEach((fk, i) => {
      if (fk.source.columns.length !== fk.target.columns.length)
        throw new Error(
          `Source columns ${JSON.stringify(fk.source.columns)} and target columns ${JSON.stringify(
            fk.target.columns
          )} must be of equal length for FK #${i + 1} in table '${statement.table}'`
        )
    })

  if (statement.columns.filter(column => column.primaryKeyModifier !== undefined).length > 1)
    throw new Error(`More than 1 PK defined for table '${statement.table}'`)

  const primaryKeyColumns: PkMetadataType =
    statement.primaryKey ??
    filterMap(statement.columns, column => column.primaryKeyModifier !== undefined && column.name)

  database.tables[statement.table] = {
    metadata: {
      name: statement.table,
      columns: statement.columns,
      primaryKey: primaryKeyColumns.length > 0 ? primaryKeyColumns : undefined,
      foreignKeys: statement.foreignKeys,
      indexes:
        primaryKeyColumns.length === 0
          ? {}
          : {
              primaryKey: {
                name: 'primaryKey',
                unique: true,
                columns: primaryKeyColumns,
              },
            },
    },
    rows: [],
    indexes: primaryKeyColumns.length === 0 ? {} : { primaryKey: undefined },
  }
}

const executeDropTable = (database: DatabaseType, statement: DropTableType) => {
  if (database.tables[statement.table] === undefined)
    throw new Error(`Error dropping table '${statement.table}' (table does not exist)`)

  delete database.tables[statement.table]
}

const executeCreateIndex = (database: DatabaseType, statement: CreateIndexType) => {
  const indexName = statement.name
  const tableName = statement.table
  const table = database.tables[tableName]

  if (database.tables[tableName] === undefined)
    throw new Error(`Error creating index '${indexName}' on table '${tableName}' (table does not exist)`)

  database.tables[tableName].metadata.indexes[indexName] = {
    name: indexName,
    unique: statement.unique,
    columns: statement.columns,
  }

  database.tables[tableName].indexes[indexName] = undefined

  // Fill this index's data with all table rows.
  updateTableIndexes(database, table, 0, table.rows.length, indexName)
}

const executeDropIndex = (database: DatabaseType, statement: DropIndexType) => {
  if (statement.name === 'primaryKey')
    throw new Error(`Error dropping index '${statement.name}' (primary key indexes cannnot be dropped)`)

  const matchedTable = Object.values(database.tables).find(table => statement.name in table.metadata.indexes)

  if (!matchedTable) throw new Error(`Error dropping index '${statement.name}' (index does not exist)`)

  delete matchedTable.metadata.indexes[statement.name]
  delete matchedTable.indexes[statement.name]
}

const generateIndexKeyEntry = (value: ScalarType) =>
  typeof value === 'number' ? leftFillNumber(value, MAX_NUMERIC_DIGITS) : value

const generateIndexKey = (values: ScalarType[]) =>
  values.length === 1 ? values[0] : JSON.stringify(values.map(generateIndexKeyEntry))

const updateTableIndexForRow = (
  database: DatabaseType,
  table: TableType,
  indexName: string,
  row: RowType,
  rowNumber: number
) => {
  const indexMetadata: IndexMetadataType = table.metadata.indexes[indexName]
  let indexData: IndexDataType | undefined = table.indexes[indexName]

  const columnValues = indexMetadata.columns.map(column => {
    const columnMetadata = getColumnMetadata(database, table.metadata.name, column)

    return row[columnMetadata.offset]
  })

  const indexKey = generateIndexKey(columnValues)

  if (indexData) {
    if (!indexData.contains(indexKey)) indexData.add(indexKey, [])
  } else {
    indexData = table.indexes[indexName] = new AvlTree<ScalarType, number[]>(indexKey, [])
  }

  const indexNode = indexData.find(indexKey)

  if (!indexNode)
    throw new Error(`Unexpected error: entry not found in index '${indexName}' for index key '${indexKey}'`)

  // If index has an UNIQUE constraint, check if it will eventually be violated for this row.
  if (indexMetadata.unique && indexNode.value.length > 0) {
    // By default, NULL values in a unique index are not considered equal, allowing multiple NULLs in the index.
    const containsAnyNullValues = columnValues.some(value => value === null)

    if (!containsAnyNullValues)
      throw new Error(
        `Index unique constraint violated for table '${
          table.metadata.name
        }' and index '${indexName}' in row '${JSON.stringify(
          row
        )}' with index key '${indexKey}' (which does not contain any NULL values)`
      )
  }

  indexNode.value.push(rowNumber)
}

const findMatchingIndex = memoize(
  (database: DatabaseType, table: string, columns: string[]): string | undefined => {
    for (const indexMetadata of Object.values(database.tables[table].metadata.indexes)) {
      if (
        columns.length === indexMetadata.columns.length &&
        columns.every((column, i) => indexMetadata.columns[i] === column)
      )
        return indexMetadata.name
    }
  }
)

export const executeInsert = (database: DatabaseType, statement: InsertType) => {
  const table = database.tables[statement.table]
  const tableMetadata = getTableMetadataByName(database, statement.table)
  const startRow = table.rows.length

  statement.rows.forEach((row, i) => {
    debug(() => i % 1024e2 === 0 && `${Humanity.binarySuffix(i)} rows inserted.`)

    // Initialize all row's columns with nulls.
    const reorderedRow: RowType = Array(table.metadata.columns.length).fill(null)

    // Initialize and rearrange row according to columns' metadata.
    row.forEach((value, i) => {
      const columnMetadata = getColumnMetadata(database, statement.table, statement.columns[i])

      // NULL value supplied to required column?
      if (value === null && !columnMetadata.nullable)
        throw new Error(`NULL value supplied to required (non-nullable) column "${statement.columns[i]}"`)

      // Coerse value (i.e., basically transform 'true' to true and 'false' to false). Other coersions might be implemented in the future.
      reorderedRow[columnMetadata.offset] = coerse(value, columnMetadata.type)
    })

    // Find all columns not present in the INSERT statement, but which are required (non-nullable) and contain no DEFAULT values.
    const invalidMissingColumns = tableMetadata.columns.filter(
      columnMetadata =>
        !columnMetadata.nullable &&
        columnMetadata.default === undefined &&
        statement.columns.indexOf(columnMetadata.name) === -1
    )

    if (invalidMissingColumns.length > 0)
      throw new Error(
        `The following non-nullable columns are missing from the INSERT statement and have no default values: ${JSON.stringify(
          invalidMissingColumns.map(column => column.name)
        )}`
      )

    // Find columns not present in the INSERT statement which contain DEFAULT values and add those defaults to the rearranged row, which
    // include auto-incremented columns.
    tableMetadata.columns.forEach(columnMetadata => {
      if (columnMetadata.default !== undefined && statement.columns.indexOf(columnMetadata.name) === -1) {
        reorderedRow[columnMetadata.offset] = columnMetadata.default
      }

      // Increment the `default` prop for auto-incremented columns, if applicable.
      if (
        columnMetadata.primaryKeyModifier?.isAutoIncremented &&
        (reorderedRow[columnMetadata.offset] as number) >= (columnMetadata.default as number)
      )
        columnMetadata.default = (reorderedRow[columnMetadata.offset] as number) + 1 // Notice these columns are always numeric.
    })

    // Check for any eventual FK constraints violations, in order to keep "referential integrity".
    if (tableMetadata.foreignKeys !== undefined) {
      tableMetadata.foreignKeys.forEach(fk => {
        const fkValues = fk.source.columns.map(column => {
          const columnMetadata = getColumnMetadata(database, statement.table, column)

          return reorderedRow[columnMetadata.offset]
        })

        if (fkValues.every(value => value === null)) return

        const indexName = findMatchingIndex(database, fk.target.table, fk.target.columns)

        if (indexName === undefined)
          throw new Error(
            `Index not found in table '${fk.target.table}' for column(s) ${JSON.stringify(fk.target.columns)}`
          )

        if (!database.tables[fk.target.table].metadata.indexes[indexName].unique)
          throw new Error(
            `Index '${indexName}' in table '${fk.target.table}' must be unique to be used as a FK`
          )

        const matchingPk: number[] | undefined = database.tables[fk.target.table].indexes[indexName]?.find(
          generateIndexKey(fkValues)
        )?.value

        if (matchingPk === undefined || matchingPk.length === 0)
          throw new Error(
            `FK constraint for column(s) ${JSON.stringify(fk.source.columns)} violated in table '${
              statement.table
            }' in row #${i}: ${JSON.stringify(row)}`
          )
      })
    }

    table.rows.push(reorderedRow)
  })

  updateTableIndexes(database, table, startRow, statement.rows.length)
}

const updateTableIndexes = (
  database: DatabaseType,
  table: TableType,
  startRow: number,
  rowCount: number,
  specificIndexName?: string
) => {
  setDebugMode(true)

  debug(() => 'Updating indexes...')

  const affectedRows: [rowNumber: number, row: RowType][] = []

  for (let i = 0; i < rowCount; i++) affectedRows[i] = [i + startRow, table.rows[i + startRow]]

  Object.keys(table.metadata.indexes)
    .filter(index => (specificIndexName !== undefined ? index === specificIndexName : true))
    .forEach(indexName => {
      debug(() => `Index: ${indexName}`)

      const indexMetadata = table.metadata.indexes[indexName]
      const columnsOffsets = indexMetadata.columns.map(
        column => getColumnMetadata(database, table.metadata.name, column).offset
      )

      debug(() => 'Sorting rows based on index.')

      const sortedRows = affectedRows.sort((left, right) => {
        const leftValue = generateIndexKey(columnsOffsets.map(i => left[1][i]))
        const rightValue = generateIndexKey(columnsOffsets.map(i => right[1][i]))

        return compare(leftValue, rightValue)
      })

      debug({ sortedRows })

      forEach(sortedRows, ([rowNumber, row], i) => {
        debug(() => i % 1024e2 === 0 && `${Humanity.binarySuffix(i)} rows processed.`)

        updateTableIndexForRow(database, table, indexName, row, rowNumber)
      })
    })

  setDebugMode(false)
}

const buildJoinResultTable = (
  database: DatabaseType,
  statement: SelectType,
  joinResult: JoinedTablesItemType
) => {
  const resultTable: ResultTableType = [[]] // The [] in the 1st item is necessary only to conform to the type, but will be replaced right after.

  const tablesMetadata = joinResult.tables.map(
    table => [table, getTableMetadataByNameOrAlias(database, statement.tables, table)] as const
  )

  resultTable[0] = tablesMetadata.flatMap(([tableNameOrAlias, metadata]) =>
    database.tables[metadata.name].metadata.columns.map(column => `${tableNameOrAlias}.${column.name}`)
  )

  Object.values(joinResult.rows).map(row =>
    resultTable.push(getRowValues(database, statement, joinResult.tables, row))
  )

  return resultTable
}

const executeSelect = (database: DatabaseType, statement: SelectType) => {
  let joinResult: JoinedTablesItemType = joinTablesAndApplyWhereConditions(database, statement)

  debug(() => 'Before sorting:')
  debug({ joinResult })

  joinResult = sortRows(database, statement, joinResult)

  debug(() => 'After sorting:')
  debug({ joinResult })

  const queryResult: ResultTableType = buildJoinResultTable(database, statement, joinResult)

  debug(() => 'Before selecting output columns:')
  debug({ queryResult })

  return selectOutputColumns(database, statement, queryResult)
}

const executeParsedSqlStatement = (database: DatabaseType, statement: SqlStatementType) => {
  debug({ statement })

  switch (statement.type) {
    case 'CreateTable':
      executeCreateTable(database, statement)
      break
    case 'DropTable':
      executeDropTable(database, statement)
      break
    case 'CreateIndex':
      executeCreateIndex(database, statement)
      break
    case 'DropIndex':
      executeDropIndex(database, statement)
      break
    case 'Insert':
      executeInsert(database, statement)
      break
    case 'Select':
      return elapsedTimes(() => executeSelect(database, statement), {
        message: `SELECT ... FROM ${statement.tables.map(table => table.name)} WHERE ...`,
        repeat: 3,
      })
    default: {
      const exhaustiveCheck: never = statement
      throw new Error(`exhaustiveCheck: ${exhaustiveCheck}`)
    }
  }
}
