import { ScalarType } from './sql_parsers.ts'
import { ResultTableType } from './sql_runtime.ts'

export let debugMode = false

export const setDebugMode = (enabled: boolean) => {
  debugMode = enabled
}

export const log = console.log

// deno-lint-ignore no-explicit-any
export const inspect = (value: any) =>
  Deno.inspect(value, {
    depth: 999,
    colors: true,
    strAbbreviateSize: Number.MAX_SAFE_INTEGER,
  }) as unknown as string
export const printObject = (value: object) => log(inspect(value))

export const sample = <T>(collection: T[]): T => collection[Math.floor(Math.random() * collection.length)]

export const shuffle = <T>(collection: T[]): T[] => {
  const results = [...collection]

  for (let i = results.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[results[i], results[j]] = [results[j], results[i]]
  }

  return results
}

export const rand = (limit: number): number => Math.floor(Math.random() * limit)

export const toRange = (count: number, { start = 0, step = 1 } = {}) => {
  const results = Array(count)

  for (let i = 0; i < count; i++) results[i] = start + i * step

  return results
}

export const timesMap = <T>(n: number, fn: (index: number) => T): T[] => map(Array(n), (_, i) => fn(i))
export const timesForEach = <T>(n: number, fn: (index: number) => T): void =>
  forEach(Array(n), (_, i) => fn(i))

export const removeDiacrytics = (text: string) => text.normalize('NFD').replace(/\p{Diacritic}/gu, '')

export const delimitAsString = (text: string): string => "'" + text.replaceAll("'", '`') + "'"

export const formatRowsForSqlInsertion = (rows: unknown[][]): string =>
  rows.map(row => '(' + row.join(', ') + ')').join(', \n')

// left & right
export const arrayIntersection = <T>(left: T[], right: T[]): T[] => filter(left, item => right.includes(item))

// left - right
export const arrayDifference = <T>(left: T[], right: T[]): T[] => filter(left, item => !right.includes(item))

export const matrixIntersection = <T>(matrix: T[][]): T[] =>
  matrix.length === 0 ? [] : reduce(matrix, (memo, rows) => arrayIntersection(memo, rows))

export const uniqueValues = <T>(collection: T[]): T[] => [...new Set(collection)]

// deno-lint-ignore no-explicit-any
export const jsonStringifyReplacer = (_key: string, value: any): any => {
  if (value instanceof Set) {
    return [...value]
  } else if (value === undefined) {
    return '(undefined)'
  } else if (value === null) {
    return '(null)'
  } else {
    return value
  }
}

// The function below is based on Ruby's filter_map():
//
// > [1, 2, 3, 4, 5].filter_map { |i| i ** 2 if i.even? }
// => [4, 16]
//
// Which would be written as:
//
// > filterMap([1, 2, 3, 4, 5], i => { if (i % 2 === 0) return i ** 2 })
// [ 4, 16 ]
//
// Or:
//
// > filterMap([1, 2, 3, 4, 5], i => i % 2 === 0 && i ** 2)
// [ 4, 16 ]
export const filterMap = <T, U>(
  collection: T[],
  fn: (item: T, index: number) => U | undefined | null | false
): U[] => {
  const results: U[] = []

  for (let i = 0; i < collection.length; i++) {
    const mappedItem = fn(collection[i], i)

    if (mappedItem !== undefined && mappedItem !== null && mappedItem !== false) results.push(mappedItem)
  }

  return results
}

export const forEach = <T>(collection: T[], fn: (item: T, index: number) => void): void => {
  for (let i = 0; i < collection.length; i++) fn(collection[i], i)
}

export const map = <T, U>(collection: T[], fn: (item: T, index: number) => U): U[] => {
  const results: U[] = Array(collection.length)

  for (let i = 0; i < collection.length; i++) results[i] = fn(collection[i], i)

  return results
}

export const reduce = <T, U>(
  collection: T[],
  fn: (acc: U, item: T, index: number) => U,
  initialAcc?: U
): U => {
  const startIndex = initialAcc === undefined ? 1 : 0
  let acc = initialAcc ?? (collection[0] as unknown as U)

  for (let i = startIndex; i < collection.length; i++) acc = fn(acc, collection[i], i)

  return acc
}

export const filter = <T>(collection: T[], fn: (item: T, index: number) => boolean): T[] => {
  const results: T[] = []

  for (let i = 0; i < collection.length; i++) {
    const item = collection[i]

    if (fn(item, i)) results.push(item)
  }

  return results
}

export const elapsedTimes = <T>(
  fn: () => T,
  { message = '(no description)', repeat = 1, matchResults = true } = {}
): T => elapsedTimesWithStats(fn, { message, repeat, matchResults }).result

export const elapsedTimesWithStats = <T>(
  fn: () => T,
  { message = '(no description)', repeat = 1, matchResults = true } = {}
) => {
  const runs = timesMap(repeat, () => {
    const [startTime, fnResult, endTime] = [Date.now(), fn(), Date.now()]

    return { result: fnResult, elapsedTime: endTime - startTime }
  })

  if (matchResults && repeat > 1) {
    const resultsAsJson = runs.map(({ result }) => JSON.stringify(result, jsonStringifyReplacer))

    if (resultsAsJson.slice(1).some(result => result !== resultsAsJson[0]))
      throw new Error(`Results don't match for '${message}'. Please supply a pure function!`)
  }

  const elapsedTimes = runs.map(({ elapsedTime }) => elapsedTime)
  const minElapsedTime = Math.min(...elapsedTimes)
  const maxElapsedTime = Math.max(...elapsedTimes)
  const avgElapsedTime = elapsedTimes.reduce((acc, time) => acc + time) / elapsedTimes.length

  log(
    '========================================================================================================================'
  )
  log(`Elapsed times for '${message}' after ${repeat} run(s):`, {
    minElapsedTime,
    maxElapsedTime,
    avgElapsedTime,
  })
  log(
    '========================================================================================================================'
  )

  return {
    result: runs[0].result,
    stats: {
      minElapsedTime,
      maxElapsedTime,
      avgElapsedTime,
    },
  }
}

// deno-lint-ignore no-explicit-any
type MemoizableFnType<T> = (...args: any[]) => T

export const memoize = <T>(fn: MemoizableFnType<T>): MemoizableFnType<T> => {
  const cache: { key: unknown[]; value: T }[] = []

  const memoizedFn: MemoizableFnType<T> = (...args) => {
    let value: T

    const entry = cache.find(({ key }) => key.every((param, i) => param === args[i]))

    if (entry !== undefined) {
      value = entry.value
    } else {
      value = fn(...args)

      cache.unshift({ key: args, value })
    }

    return value
  }

  return memoizedFn
}

// const memoize = <T>(fn: MemoizableFnType<T>): MemoizableFnType<T> => {
//   const cache: { [key: string]: T } = {}
//
//   const memoizedFn: MemoizableFnType<T> = (...args) => {
//     if (args.some(arg => typeof arg === 'function'))
//       throw new Error(
//         'Sorry, this function has at least 1 function argument and cannot be memoized!'
//       )
//
//     const key = JSON.stringify(args)
//
//     if (!(key in cache)) cache[key] = fn(...args)
//
//     return cache[key]
//   }
//
//   return memoizedFn
// }

// > const a = [['a', 'b', 'c', 'd', 'e'], [1, 2, 3, 4, 5], [6, 7, 8, 9, 10]]
// > console.table(convertMatrixToObjectForm(a))
// ┌───────┬───┬───┬───┬───┬────┐
// │ (idx) │ a │ b │ c │ d │ e  │
// ├───────┼───┼───┼───┼───┼────┤
// │     0 │ 1 │ 2 │ 3 │ 4 │  5 │
// │     1 │ 6 │ 7 │ 8 │ 9 │ 10 │
// └───────┴───┴───┴───┴───┴────┘
export const convertMatrixToObjectForm = (matrix: [string[], ...ScalarType[][]]) => {
  const [head, ...tail] = matrix

  return tail.map(row =>
    row.reduce(
      (memo: { [name: string]: ScalarType }, value, i) => tap(memo, acc => (acc[head[i]] = value)),
      {}
    )
  )
}

export const printTable = (table: ResultTableType) => console.table(convertMatrixToObjectForm(table))

export const tap = <T>(value: T, fn: (sameValue: T) => void): T => {
  fn(value)

  return value
}

export const debug = (output: object | (() => string | false)): void => {
  if (debugMode) {
    if (typeof output === 'function') {
      const message = output()

      if (message === false) return

      log(message)
    } else printObject(output)

    log('-----------------------------------')
  }
}

export const isArrayEmpty = (collection: unknown[]): boolean => !collection.some(() => true)
export const arrayNonEmptyItemsCount = (collection: unknown[]): number => Object.keys(collection).length

export const leftFillNumber = (value: number, targetLength: number, paddingChar = '0') => {
  const textPositiveValue = Math.abs(value).toString()

  const result =
    textPositiveValue.length > targetLength
      ? textPositiveValue
      : textPositiveValue.padStart(targetLength, paddingChar)

  return value >= 0 ? result : '-' + result
}

type NonUndefined<T> = NonNullable<T> | null

export const compare = <T>(leftValue: NonUndefined<T>, rightValue: NonUndefined<T>) => {
  if (leftValue === null) {
    if (rightValue === null) return 0
    else return -1 // A left NULL value is always smaller than a right non-NULL one.
  } else {
    if (rightValue === null) return 1 // A left non-NULL value is always greater than a right NULL one.
    else return leftValue < rightValue ? -1 : leftValue > rightValue ? 1 : 0
  }
}

export const openFilenames = async (filenameOrFilenames: string | string[]) =>
  await new Deno.Command('open', {
    args: Array.isArray(filenameOrFilenames) ? filenameOrFilenames : [filenameOrFilenames],
  }).output()
