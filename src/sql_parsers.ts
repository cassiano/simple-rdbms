import {
  and,
  and3,
  and4,
  and5,
  andN,
  boolean,
  charSequence,
  closeParens,
  comma,
  concat,
  delimitedBy,
  EMPTY_STRING,
  identifier,
  joinedBy,
  many1,
  map,
  naturalGreaterThanZero,
  numeric,
  openParens,
  optional,
  or,
  or3,
  or4,
  orN,
  Parser,
  period,
  precededBy,
  separatedBy,
  SPACE,
  spaced,
  string,
  succeededBy,
} from './parser_combinators.ts'

const AUTOINCREMENT_INITIAL_VALUE = 1

const keyword = <T extends string>(text: T) =>
  concat(andN(text.split(' ').map(part => spaced(charSequence(part.trim())))), SPACE) as Parser<T>
const keywords = keyword

const as = keyword('AS')
const nullOption = keyword('NULL')
const notNullOption = keywords('NOT NULL')
const isNull = keywords('IS NULL')
const isNotNull = keywords('IS NOT NULL')

export const SQL_TYPES = ['INTEGER', 'VARCHAR', 'BOOLEAN'] as const
export const ASTERISK = '*'
export const COUNT_ASTERISK = 'COUNT(*)'
export const SEMICOLON = ';'
export const SQL_CONDITIONAL_OPERATORS = ['=', '<>', '<=', '<', '>=', '>'] as const
export const SQL_DIRECTIONS = ['ASC', 'DESC'] as const

const countAsterisk = keyword(COUNT_ASTERISK)

export type SqlDatatypeType = (typeof SQL_TYPES)[number]
export type NameWithOptionalAliasType = { name: string; alias?: string }
export type SqlColumnWithOptionalAliasType = [alias: string, name: string]
export type ScalarType = number | string | boolean | null
export type SqlConditionalOperatorType = (typeof SQL_CONDITIONAL_OPERATORS)[number]
export type SqlDirectionType = (typeof SQL_DIRECTIONS)[number]
export type RowType = ScalarType[]

const defaultIfBlank = <T, U, V>(
  expression: T | typeof EMPTY_STRING,
  value?: U,
  defaultValue: V | undefined = undefined
) => (expression === EMPTY_STRING ? defaultValue : value ?? expression)

const emptyObjectIfBlank = <T, U>(expression: T | typeof EMPTY_STRING, value?: U) =>
  defaultIfBlank(expression, value, {})

export type ColumnMetadataType = {
  offset: number
  name: string
  type: SqlDatatypeType
  nullable: boolean
  default?: ScalarType
  primaryKeyModifier?: {
    isAutoIncremented: boolean
  }
}

export type SqlColumnType = { type: 'column'; value: NameWithOptionalAliasType }
export type SqlScalarType = { type: 'scalar'; value: ScalarType }
export type SqlExpressionType = SqlColumnType | SqlScalarType

export type SqlConditionType = {
  left: SqlExpressionType
  operator: SqlConditionalOperatorType
  right: SqlExpressionType
}

export type ColumnOrderType = {
  column: NameWithOptionalAliasType | number
  direction?: SqlDirectionType
}

export type FkMetadataType = {
  source: {
    columns: string[]
  }
  target: {
    table: string
    columns: string[]
  }
}

export type PkMetadataType = string[]

export type CreateTableType = {
  type: 'CreateTable'
  table: string
  columns: ColumnMetadataType[]
  primaryKey?: PkMetadataType
  foreignKeys?: FkMetadataType[]
}

export type DropTableType = {
  type: 'DropTable'
  table: string
}

export type CreateIndexType = {
  type: 'CreateIndex'
  name: string
  unique: boolean
  table: string
  columns: string[]
}

export type DropIndexType = {
  type: 'DropIndex'
  name: string
}

export type InsertType = {
  type: 'Insert'
  table: string
  columns: string[]
  rows: RowType[]
}

export type SelectType = {
  type: 'Select'
  columns: typeof ASTERISK | typeof COUNT_ASTERISK | NameWithOptionalAliasType[]
  tables: NameWithOptionalAliasType[]
  where?: SqlConditionType[]
  orderBy?: ColumnOrderType[]
}

export type SqlStatementType =
  | CreateTableType
  | DropTableType
  | CreateIndexType
  | DropIndexType
  | SelectType
  | InsertType

export const coerse = (value: ScalarType, type: SqlDatatypeType) =>
  type === 'BOOLEAN' ? String(value).toLowerCase() === 'true' : value

export const nullValue = map(keyword('NULL'), _ => null)

export const asterisk = keyword(ASTERISK)
export const semicolon = keyword(SEMICOLON)
export const sqlScalarValue: Parser<ScalarType> = or4(numeric, string, boolean, nullValue)
export const sqlDatatype = orN(SQL_TYPES.map(keyword)) as Parser<(typeof SQL_TYPES)[number]>

const operand: Parser<ScalarType | SqlColumnWithOptionalAliasType> = or(
  sqlScalarValue,
  and(optional(succeededBy(identifier, period)), identifier)
)
const operator = orN(SQL_CONDITIONAL_OPERATORS.map(keyword)) as Parser<SqlConditionalOperatorType>
const columnNameWithOptionalAlias = and(optional(succeededBy(identifier, period)), identifier)
const tableNameWithOptionalAlias = and(identifier, optional(precededBy(as, spaced(identifier))))

export type SqlConditionOperandType =
  | { type: 'column'; value: NameWithOptionalAliasType }
  | { type: 'scalar'; value: ScalarType }

const buildWhereCondition = (
  left: ScalarType | [string, string],
  operator: SqlConditionalOperatorType,
  right: ScalarType | [string, string]
): {
  left: SqlConditionOperandType
  operator: SqlConditionalOperatorType
  right: SqlConditionOperandType
} => ({
  left: Array.isArray(left)
    ? {
        type: 'column',
        value: {
          alias: defaultIfBlank(left[0]),
          name: left[1],
        },
      }
    : { type: 'scalar', value: left },
  operator,
  right: Array.isArray(right)
    ? {
        type: 'column',
        value: {
          alias: defaultIfBlank(right[0]),
          name: right[1],
        },
      }
    : { type: 'scalar', value: right },
})

const columnDefinition = and5(
  identifier,
  sqlDatatype,
  optional(or(nullOption, notNullOption)),
  optional(precededBy(keyword('DEFAULT'), sqlScalarValue)),
  optional(and(keywords('PRIMARY KEY'), optional(keyword('AUTOINCREMENT'))))
)

export const createTable: Parser<CreateTableType> = map(
  succeededBy(
    and(
      precededBy(keywords('CREATE TABLE'), identifier),
      delimitedBy(
        openParens,
        and3(
          separatedBy(columnDefinition, comma),
          optional(
            precededBy(
              precededBy(comma, keywords('PRIMARY KEY')),
              delimitedBy(openParens, separatedBy(identifier, comma), closeParens)
            )
          ),
          optional(
            precededBy(
              comma,
              separatedBy(
                precededBy(
                  keywords('FOREIGN KEY'),
                  and3(
                    delimitedBy(openParens, separatedBy(identifier, comma), closeParens),
                    precededBy(keyword('REFERENCES'), identifier),
                    delimitedBy(openParens, separatedBy(identifier, comma), closeParens)
                  )
                ),
                comma
              )
            )
          )
        ),
        closeParens
      )
    ),
    optional(semicolon)
  ),
  ([table, [columns, primaryKey, foreignKeys]]) => ({
    type: 'CreateTable' as const,
    table,
    columns: columns.map(([name, type, nullifyOption, defaultValue, primaryKeyAndAutoIncrement], offset) => ({
      offset,
      name,
      type,
      nullable: nullifyOption !== 'NOT NULL',
      default:
        primaryKeyAndAutoIncrement?.[1] !== 'AUTOINCREMENT'
          ? defaultIfBlank(defaultValue, coerse(defaultValue, type))
          : AUTOINCREMENT_INITIAL_VALUE,
      ...emptyObjectIfBlank(primaryKeyAndAutoIncrement, {
        primaryKeyModifier: {
          isAutoIncremented: primaryKeyAndAutoIncrement[1] === 'AUTOINCREMENT',
        },
      }),
    })),
    primaryKey: defaultIfBlank(primaryKey),
    foreignKeys:
      foreignKeys === '' // Using `defaultIfBlank()` would inhibit type narrowing here.
        ? undefined
        : foreignKeys.map(([sourceColumns, targetTable, targetColumns]) => ({
            source: { columns: sourceColumns },
            target: { table: targetTable, columns: targetColumns },
          })),
  })
)

export const dropTable: Parser<DropTableType> = map(
  succeededBy(precededBy(keywords('DROP TABLE'), identifier), optional(semicolon)),
  table => ({
    type: 'DropTable' as const,
    table,
  })
)

export const createIndex: Parser<CreateIndexType> = map(
  succeededBy(
    and4(
      succeededBy(precededBy(keyword('CREATE'), optional(keyword('UNIQUE'))), keyword('INDEX')),
      identifier,
      precededBy(keyword('ON'), identifier),
      delimitedBy(openParens, separatedBy(identifier, comma), closeParens)
    ),
    optional(semicolon)
  ),
  ([unique, name, table, columns]) => ({
    type: 'CreateIndex' as const,
    unique: unique === 'UNIQUE',
    name,
    table,
    columns,
  })
)

export const dropIndex: Parser<DropIndexType> = map(
  succeededBy(precededBy(keywords('DROP INDEX'), identifier), optional(semicolon)),
  name => ({
    type: 'DropIndex' as const,
    name,
  })
)

export const select: Parser<SelectType> = map(
  succeededBy(
    and4(
      precededBy(
        keyword('SELECT'),
        or3(asterisk, countAsterisk, separatedBy(columnNameWithOptionalAlias, comma))
      ),
      precededBy(keyword('FROM'), separatedBy(tableNameWithOptionalAlias, comma)),
      optional(
        precededBy(
          keyword('WHERE'),
          separatedBy(
            or3(
              map(and3(operand, operator, operand), members => ({
                type: 'binaryComparison' as const,
                members,
              })),
              map(
                and(succeededBy(operand, keyword('BETWEEN')), joinedBy(operand, keyword('AND'))),
                members => ({ type: 'ternaryComparison' as const, members })
              ),
              map(and(operand, or(isNull, isNotNull)), members => ({
                type: 'nullComparison' as const,
                members,
              }))
            ),
            keyword('AND')
          )
        )
      ),
      optional(
        precededBy(
          keywords('ORDER BY'),
          separatedBy(
            and(
              or(
                columnNameWithOptionalAlias,
                naturalGreaterThanZero // Column indexes, ≥ 1.
              ),
              optional(orN(SQL_DIRECTIONS.map(keyword)))
            ),
            comma
          )
        )
      )
    ),
    optional(semicolon)
  ),
  ([columns, tables, where, orderBy]) => ({
    type: 'Select' as const,

    columns:
      columns === ASTERISK || columns === COUNT_ASTERISK
        ? columns
        : columns.map(([alias, name]) => ({
            alias: defaultIfBlank(alias),
            name,
          })),

    tables: tables.map(([name, alias]) => ({
      alias: defaultIfBlank(alias),
      name,
    })),

    where:
      where === ''
        ? undefined
        : where.flatMap(condition => {
            switch (condition.type) {
              case 'binaryComparison': {
                const [left, operator, right] = condition.members

                return buildWhereCondition(left, operator, right)
              }

              case 'ternaryComparison': {
                const [left, [minright, maxright]] = condition.members

                return [buildWhereCondition(left, '>=', minright), buildWhereCondition(left, '<=', maxright)]
              }

              case 'nullComparison': {
                const [left, nulloperator] = condition.members

                return buildWhereCondition(left, nulloperator === 'IS NULL' ? '=' : '<>', null)
              }

              default: {
                const exhaustiveCheck: never = condition
                throw new Error(`exhaustiveCheck: ${exhaustiveCheck}`)
              }
            }
          }),

    orderBy:
      orderBy === '' // Using `defaultIfBlank()` would inhibit type narrowing here.
        ? undefined
        : orderBy.map(([column, direction]) => ({
            column: Array.isArray(column) ? { alias: defaultIfBlank(column[0]), name: column[1] } : column,
            direction: direction === '' ? undefined : (direction as SqlDirectionType),
          })),
  })
)

export const insert: Parser<InsertType> = map(
  succeededBy(
    and3(
      precededBy(keywords('INSERT INTO'), identifier),
      delimitedBy(openParens, separatedBy(identifier, comma), closeParens),
      precededBy(
        keyword('VALUES'),
        separatedBy(delimitedBy(openParens, separatedBy(sqlScalarValue, comma), closeParens), comma)
      )
    ),
    optional(semicolon)
  ),
  ([table, columns, rows]) => ({
    type: 'Insert' as const,
    table,
    columns,
    rows,
  })
)

export const ddl = orN<SqlStatementType>([createTable, dropTable, createIndex, dropIndex])
export const dml = orN<SqlStatementType>([select, insert])

export const sqlStatement = or(ddl, dml)
export const sqlStatements = many1(sqlStatement)
