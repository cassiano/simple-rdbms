import { compare, log } from './util.ts'

export enum VisitorType {
  PreOrder, // When passing to the left of a node
  InOrder, // When passing below a node
  PostOrder, // When passing to the right of a node
}

type SubTreeType = 'left' | 'right'

// https://www.geeksforgeeks.org/binary-search-tree-data-structure
// deno-lint-ignore ban-types
export class BST<K extends {} | null, V> {
  static blankNodeId = 0

  constructor(public key: K, public value: V, public left?: BST<K, V>, public right?: BST<K, V>) {
    if (this.left) this.validateLeftSubtree()
    if (this.right) this.validateRightSubtree()
  }

  addLeft(key: K, value: V, skipValidation = false): BST<K, V> {
    this.left = new BST(key, value)

    if (!skipValidation) this.validateLeftSubtree()

    return this.left
  }

  addRight(key: K, value: V, skipValidation = false): BST<K, V> {
    this.right = new BST(key, value)

    if (!skipValidation) this.validateRightSubtree()

    return this.right
  }

  isLeaf(): boolean {
    return !this.left && !this.right
  }

  leafCount(): number {
    if (this.isLeaf()) return 1

    const leftLeafCount = this.left?.leafCount() ?? 0
    const rightLeafCount = this.right?.leafCount() ?? 0

    return leftLeafCount + rightLeafCount
  }

  height(): number {
    const leftHeight = this.left?.height() ?? 0
    const rightHeight = this.right?.height() ?? 0

    return Math.max(leftHeight, rightHeight) + 1
  }

  count(): number {
    const leftCount = this.left?.count() ?? 0
    const rightCount = this.right?.count() ?? 0

    return leftCount + rightCount + 1
  }

  leftMost(): BST<K, V> {
    return this.left?.leftMost() ?? this
  }

  rightMost(): BST<K, V> {
    return this.right?.rightMost() ?? this
  }

  add(key: K, value: V): void {
    const comparisonResult = compare(key, this.key)

    switch (comparisonResult) {
      case -1 /* key < this.key */:
        if (this.left) this.left.add(key, value)
        else this.addLeft(key, value, true)
        break

      case 1 /* key > this.value */:
        if (this.right) this.right.add(key, value)
        else this.addRight(key, value, true)
        break

      case 0 /* key === this.value */:
        throw new Error(`Duplicate key '${key}' not allowed in BST tree'`)

      default: {
        // https://www.typescriptlang.org/docs/handbook/2/narrowing.html#exhaustiveness-checking
        const exhaustiveCheck: never = comparisonResult
        throw exhaustiveCheck
      }
    }
  }

  // https://www.geeksforgeeks.org/deletion-in-binary-search-tree/
  delete(key: K, parent?: BST<K, V>, subtreeProp?: SubTreeType): boolean {
    const comparisonResult = compare(key, this.key)

    if (comparisonResult === -1 /* key < this.key */) return this.left?.delete(key, this, 'left') ?? false
    else if (comparisonResult === 1 /* key > this.key */)
      return this.right?.delete(key, this, 'right') ?? false

    // valueToDelete === this.value
    if (this.isLeaf()) {
      if (parent && subtreeProp) {
        parent[subtreeProp] = undefined

        return true
      }

      throw 'Trying to delete a top level root!'
    }

    if (this.left) {
      // Find inorder predecessor.
      const predecessor = this.left.rightMost() as BST<K, V>
      const predecessorValue = predecessor.key

      // Delete inorder predecessor and replace the current root's value (that we are effectively
      // trying to delete) with it.
      if (this.left.delete(predecessor.key, this, 'left')) {
        this.key = predecessorValue

        return true
      }

      throw `Value ${predecessor.key} could not be deleted from left subtree ${this.left.key}`
    }

    // It current root is not a leaf and does not contain a left subtree, then it MUST necessarily
    // contain a right subtree, so it is perfectly safe to eliminate `undefined` from the union
    // type `BST<T> | undefined`.
    this.right = this.right!

    // Find inorder successor.
    const successor = this.right.leftMost() as BST<K, V>
    const successorValue = successor.key

    // Delete inorder successor and replace the current root's value (that we are effectively
    // trying to delete) with it.
    if (this.right.delete(successor.key, this, 'right')) {
      this.key = successorValue

      return true
    }

    throw `Key ${successor.key} could not be deleted from right subtree ${this.right.key}`
  }

  min(): BST<K, V> {
    return this.leftMost()
  }

  max(): BST<K, V> {
    return this.rightMost()
  }

  contains(value: K): boolean {
    return !!this.find(value)
  }

  find(key: K): BST<K, V> | undefined {
    const comparisonResult = compare(key, this.key)

    switch (comparisonResult) {
      case -1 /* key < this.key */:
        return this.left?.find(key)
      case 1 /* key > this.value */:
        return this.right?.find(key)
      case 0 /* key === this.value */:
        return this
      default: {
        // https://www.typescriptlang.org/docs/handbook/2/narrowing.html#exhaustiveness-checking
        const exhaustiveCheck: never = comparisonResult
        throw exhaustiveCheck
      }
    }
  }

  *iterator(type: VisitorType = VisitorType.InOrder): Generator<BST<K, V>, void, void> {
    if (type === VisitorType.PreOrder) yield this
    if (this.left) yield* this.left.iterator(type)
    if (type === VisitorType.InOrder) yield this
    if (this.right) yield* this.right.iterator(type)
    if (type === VisitorType.PostOrder) yield this
  }

  *[Symbol.iterator](): Generator<BST<K, V>, void, void> {
    yield* this.iterator() // Uses default order (Inorder).
  }

  print(level = 0, showIndentationGuides: boolean[] = []): void {
    const SUBTREE_CONNECTOR = '─ x ─┐'

    const indentation = showIndentationGuides.reduce(
      (acc, showIndentationGuide) =>
        acc + (showIndentationGuide ? '|' : ' ') + ' '.repeat(SUBTREE_CONNECTOR.length - 1),
      ''
    )

    log(indentation + `${this.key}: ${JSON.stringify(this.value)}`)

    if (this.left) {
      log(indentation + (this.right ? '├' : '└') + SUBTREE_CONNECTOR.replaceAll('x', '←'))

      this.left.print(level + 1, [...showIndentationGuides, !!this.right])
    }

    if (this.right) {
      log(indentation + '└' + SUBTREE_CONNECTOR.replaceAll('x', '→'))

      this.right.print(level + 1, [...showIndentationGuides, false])
    }
  }

  isValid(): boolean {
    return (
      (this.left
        ? compare(this.left.key, this.key) <= 0 /* this.left.key <= this.key */ && this.left.isValid()
        : true) &&
      (this.right
        ? compare(this.right.key, this.key) === 1 /* this.right.key > this.key */ && this.right.isValid()
        : true)
    )
  }

  lt(maxKey: K, { inclusive = false } = {}): BST<K, V>[] {
    const comparisonResult = compare(this.key, maxKey)

    if (comparisonResult === 1 /* this.key > maxKey */)
      // Delegate the search entirely to the left subtree, if existent.
      return this.left?.lt(maxKey, { inclusive }) ?? []

    const leftNodesInOrder = [...(this.left ?? [])]

    if (comparisonResult === 0 /* this.key === maxKey */)
      return inclusive ? [...leftNodesInOrder, this] : leftNodesInOrder

    // this.key < maxKey
    return [...leftNodesInOrder, this, ...(this.right?.lt(maxKey, { inclusive }) ?? [])]
  }

  lte(maxKey: K): BST<K, V>[] {
    return this.lt(maxKey, { inclusive: true })
  }

  gt(minKey: K, { inclusive = false } = {}): BST<K, V>[] {
    const comparisonResult = compare(this.key, minKey)

    if (comparisonResult === -1 /* this.key < minKey */)
      // Delegate the search entirely to the right subtree, if existent.
      return this.right?.gt(minKey, { inclusive }) ?? []

    const rightNodesInOrder = [...(this.right ?? [])]

    if (comparisonResult === 0 /* this.key === minKey */)
      return inclusive ? [this, ...rightNodesInOrder] : rightNodesInOrder

    // this.key > minKey
    return [...(this.left?.gt(minKey, { inclusive }) ?? []), this, ...rightNodesInOrder]
  }

  gte(minKey: K): BST<K, V>[] {
    return this.gt(minKey, { inclusive: true })
  }

  // Poor man's version:
  //
  // between(minKey: K, maxKey: K): BST<K, V>[] {
  //   return arrayIntersection(this.gte(minKey), this.lte(maxKey))
  // }

  // ≥ minKey and ≤ maxKey
  between(minKey: K, maxKey: K, { inclusive = true } = {}): BST<K, V>[] {
    if (compare(minKey, maxKey) === 1 /* !(minKey <= maxKey) */)
      throw new Error(`Minimum key (${minKey}) must be ≤ maximum key (${maxKey})`)

    if (compare(this.key, maxKey) === 1 /* this.key > maxKey */)
      // Delegate the search entirely to the left subtree, if existent.
      return this.left?.between(minKey, maxKey, { inclusive }) ?? []

    if (compare(this.key, minKey) === -1 /* this.key < minKey */)
      // Delegate the search entirely to the right subtree, if existent.
      return this.right?.between(minKey, maxKey, { inclusive }) ?? []

    // minKey ≤ this.key ≤ maxKey
    return [
      ...(this.left?.gt(minKey, { inclusive }) ?? []),
      this,
      ...(this.right?.lt(maxKey, { inclusive }) ?? []),
    ]
  }

  async toGraphViz(filename = 'avl_tree') {
    // https://graphviz.org/doc/info/attrs.html
    const dot = `
      digraph {
        dpi=192;
        rankdir=TB;
        labelloc="t";
        label="${filename}";

        # Nodes and edges.
        ${this.subtreeDot()}
      }
    `

    const tempDir = await Deno.makeTempDir()

    await Deno.writeTextFile(`${tempDir}/avl_tree.dot`, dot)

    const process = await new Deno.Command('dot', {
      args: ['-Tpng', `${tempDir}/avl_tree.dot`, '-o', `${tempDir}/${filename}.png`],
    }).output()

    if (!process.success)
      throw new Error(`Unexpected error code ${process.code} when generating tree '${filename}'`)

    return `${tempDir}/${filename}.png`
  }

  ///////////////////////
  // Protected methods //
  ///////////////////////

  protected validate(): void {
    if (!this.isValid()) throw new Error(`Tree with root '${this.key}' is invalid.`)
  }

  protected largestHeightChild(
    subTreeTypePreference: SubTreeType = 'left'
  ): [BST<K, V>, SubTreeType] | undefined {
    if (this.isLeaf()) return undefined

    const leftHeight = this.left?.height() ?? 0
    const rightHeight = this.right?.height() ?? 0

    if (leftHeight === rightHeight) return [this[subTreeTypePreference]!, subTreeTypePreference]
    else if (leftHeight > rightHeight) return [this.left!, 'left']
    // leftHeight < rightHeight
    else return [this.right!, 'right']
  }

  protected hasBothChildren() {
    return this.left && this.right
  }

  protected hasSingleChild() {
    return !this.isLeaf() && !this.hasBothChildren()
  }

  /////////////////////
  // Private methods //
  /////////////////////

  private subtreeDot(parent?: BST<K, V>): string | undefined {
    let dot = ''

    const label = this.nodeLabel()

    dot += `"${label}"; \n`

    if (parent) dot += `"${parent.nodeLabel()}" -> "${label}"; \n`

    if (!this.isLeaf()) {
      dot += this.left?.subtreeDot(this) ?? this.blankNodeDot(label)
      dot += this.right?.subtreeDot(this) ?? this.blankNodeDot(label)
    }

    return dot
  }

  private nodeLabel() {
    return `${JSON.stringify(this.key).replaceAll('"', "'")}: ${JSON.stringify(this.value)}`
  }

  private blankNodeDot(parentNodeLabel: string) {
    let dot = ''

    const blankNodeName = `blank-node-${BST.blankNodeId++}`

    dot += `"${parentNodeLabel}" -> "${blankNodeName}" [fontcolor=lightgray, color=lightgray, style=dashed]; \n`
    dot += `"${blankNodeName}" [label="", color=lightgray, style=dashed]; \n`

    return dot
  }

  private validateLeftSubtree(): void {
    if (
      this.left &&
      (!this.left.isValid() || compare(this.left.max().key, this.key)) ===
        1 /* this.left.max().key > this.key */
    )
      throw `Tree with root '${this.key}' contains an invalid left subtree with root '${this.left.key}'`
  }

  private validateRightSubtree(): void {
    if (
      this.right &&
      (!this.right.isValid() ||
        compare(this.right.min().key, this.key) === -1) /* this.right.min().key < this.key */
    )
      throw `Tree with root '${this.key}' contains an invalid right subtree with root '${this.right.key}'`
  }
}

// ----------------------------------------------------------------------------------------------------------------------------------------

enum AvlOperationArrangementType {
  LeftLeft = 'LeftLeft',
  LeftRight = 'LeftRight',
  RightRight = 'RightRight',
  RightLeft = 'RightLeft',
}

// deno-lint-ignore ban-types
export class AvlTree<K extends {} | null, V> extends BST<K, V> {
  declare left?: AvlTree<K, V>
  declare right?: AvlTree<K, V>

  addLeft(key: K, value: V, skipValidation = false): AvlTree<K, V> {
    this.left = new AvlTree(key, value)

    if (!skipValidation) this.validate()

    return this.left
  }

  addRight(key: K, value: V, skipValidation = false): AvlTree<K, V> {
    this.right = new AvlTree(key, value)

    if (!skipValidation) this.validate()

    return this.right
  }

  // Checks whether the current root is balanced or not.
  isValid(): boolean {
    if (this.isLeaf()) return true
    if (!super.isValid()) return false // Must be a valid BST.
    if (!this.isBalanced()) return false // Must be balanced.

    return true
  }

  isBalanced(): boolean {
    return AvlTree.isBalanced(this.balanceFactor())
  }

  // https://www.geeksforgeeks.org/insertion-in-an-avl-tree/
  add(key: K, value: V): void {
    const newNodeAncestors = this.bstAddReturningAncestors(key, value)

    // Find 1st unbalanced node starting from the parent of the new node all the way into
    // the root.
    let zBalanceFactor!: number
    const z = newNodeAncestors
      .reverse()
      .slice(1)
      .find(node => {
        zBalanceFactor = node.balanceFactor()

        return !AvlTree.isBalanced(zBalanceFactor)
      })

    if (!z) return // Insertion is ok when no unbalanced nodes found.

    let arrangement: AvlOperationArrangementType
    let y: AvlTree<K, V>

    if (zBalanceFactor! > 0) {
      y = z.left! // It is guaranteed that a left subtree exists.

      arrangement =
        compare(key, y.key) <= 0 /* key <= y.key */
          ? AvlOperationArrangementType.LeftLeft
          : AvlOperationArrangementType.LeftRight
    } else {
      // zBalanceFactor! < 0 (since it cannot be 0)
      y = z.right! // It is guaranteed that a right subtree exists.

      arrangement =
        compare(key, y.key) <= 0 /* key <= y.key */
          ? AvlOperationArrangementType.RightLeft
          : AvlOperationArrangementType.RightRight
    }

    z.reBalance(y, arrangement)
  }

  // https://www.geeksforgeeks.org/deletion-in-an-avl-tree/
  delete(key: K): boolean {
    const path = this.bstDeleteReturningAncestors(key)

    if (path.length === 0) return false // Key not found.

    for (const node of path.reverse()) {
      if (node.isBalanced()) continue

      const z = node
      const [y, ySubTreeType] = z.largestHeightChild()! as [AvlTree<K, V>, SubTreeType]
      const [x, _] = y.largestHeightChild(ySubTreeType)! as [AvlTree<K, V>, SubTreeType]

      let arrangement: AvlOperationArrangementType

      if (y === z.left) {
        if (x === y.left) arrangement = AvlOperationArrangementType.LeftLeft
        else if (x === y.right) arrangement = AvlOperationArrangementType.LeftRight
        else throw new Error('${x.key} is neither ${y.left.key} nor ${y.right.key}')
      } else if (y === z.right) {
        if (x === y.right) arrangement = AvlOperationArrangementType.RightRight
        else if (x === y.left) arrangement = AvlOperationArrangementType.RightLeft
        else throw new Error('${x.key} is neither ${y.right.key} nor ${y.left.key}')
      } else throw new Error('${y.key} is neither ${z.left.key} nor ${z.right.key}')

      z.reBalance(y, arrangement)
    }

    return true
  }

  /////////////////////
  // Private methods //
  /////////////////////

  private balanceFactor(): number {
    const leftHeight = this.left?.height() ?? 0
    const rightHeight = this.right?.height() ?? 0

    return leftHeight - rightHeight
  }

  // Adds a new node into the AVL tree using the BST standard algorithm, returning all ancestors
  // all way to the root (including the new node at the last position).
  private bstAddReturningAncestors(key: K, value: V, path: AvlTree<K, V>[] = []): AvlTree<K, V>[] {
    const updatedPath = [...path, this]
    const comparisonResult = compare(key, this.key)

    switch (comparisonResult) {
      case -1 /* key < this.key */:
        if (this.left) return this.left.bstAddReturningAncestors(key, value, updatedPath)
        else {
          const newNode = this.addLeft(key, value, true)

          return [...updatedPath, newNode]
        }

      case 1 /* key > this.value */:
        if (this.right) return this.right.bstAddReturningAncestors(key, value, updatedPath)
        else {
          const newNode = this.addRight(key, value, true)

          return [...updatedPath, newNode]
        }

      case 0 /* key === this.value */:
        throw new Error(`Duplicate key '${key}' not allowed in AVL tree`)

      default: {
        // https://www.typescriptlang.org/docs/handbook/2/narrowing.html#exhaustiveness-checking
        const exhaustiveCheck: never = comparisonResult
        throw exhaustiveCheck
      }
    }
  }

  // Deletes a given node from the AVL tree using the BST standard algorithm, returning all ancestors
  // all way to the root (including the effectively deleted node at the last position).
  private bstDeleteReturningAncestors(
    key: K,
    parent?: BST<K, V>,
    subtreeProp?: SubTreeType,
    path: AvlTree<K, V>[] = []
  ): AvlTree<K, V>[] {
    const comparisonResult = compare(key, this.key)

    if (comparisonResult === -1 /* key < this.key */)
      return this.left?.bstDeleteReturningAncestors(key, this, 'left', [...path, this]) ?? []
    else if (comparisonResult === 1 /* key > this.key */)
      return this.right?.bstDeleteReturningAncestors(key, this, 'right', [...path, this]) ?? []

    const isTopRootNode = parent === undefined || subtreeProp === undefined

    // key === this.key
    if (this.isLeaf()) {
      if (!isTopRootNode) {
        parent[subtreeProp] = undefined

        return path
      }

      throw new Error('Trying to delete a top level root!')
    }

    if (this.hasSingleChild()) {
      if (!isTopRootNode) {
        parent[subtreeProp] = this.left ?? this.right

        return path
      }
    }

    if (this.left) {
      // Find inorder predecessor.
      const predecessor = this.left.max()

      // Delete inorder predecessor and replace the current root's value (that we are effectively
      // trying to delete) with it.
      const updatedPath = this.left.bstDeleteReturningAncestors(predecessor.key, this, 'left', [
        ...path,
        this,
      ])

      if (updatedPath.length === 0)
        throw new Error(`Key ${predecessor.key} could not be deleted from left subtree ${this.left.key}`)

      this.key = predecessor.key
      this.value = predecessor.value

      return updatedPath
    }

    this.right = this.right! // There will always be a right subtree in this case.

    // Find inorder successor.
    const successor = this.right.min()

    // Delete inorder successor and replace the current root's value (that we are effectively
    // trying to delete) with it.
    const updatedPath = this.right.bstDeleteReturningAncestors(successor.key, this, 'right', [...path, this])

    if (updatedPath.length === 0)
      throw new Error(`Key ${successor.key} could not be deleted from right subtree ${this.right.key}`)

    this.key = successor.key
    this.value = successor.value

    return updatedPath
  }

  //      y                           x
  //     / \     Right Rotation      / \
  //    x   T3   - - - - - - - >   T1   y
  //   / \       < - - - - - - -       / \
  // T1   T2     Left Rotation       T2   T3

  private rightRotate(): void {
    if (!this.left) throw new Error(`Cannot right rotate root '${this.key}' since there is no left subtree.`)

    // deno-lint-ignore no-this-alias
    const y = this
    const x = y.left!
    const t1 = x.left
    const t2 = x.right
    const t3 = y.right

    const newY = new AvlTree(y.key, y.value, t2, t3)

    this.key = x.key
    this.value = x.value
    this.left = t1
    this.right = newY
  }

  private leftRotate(): void {
    if (!this.right) throw new Error(`Cannot left rotate root '${this.key}' since there is no right subtree.`)

    // deno-lint-ignore no-this-alias
    const x = this
    const y = x.right!
    const t1 = x.left
    const t2 = y.left
    const t3 = y.right

    const newX = new AvlTree(x.key, x.value, t1, t2)

    this.key = y.key
    this.value = y.value
    this.left = newX
    this.right = t3
  }

  private static isBalanced(balanceFactor: number): boolean {
    return Math.abs(balanceFactor) <= 1
  }

  private reBalance(subtree: AvlTree<K, V>, arrangement: AvlOperationArrangementType) {
    switch (arrangement) {
      case AvlOperationArrangementType.LeftLeft:
        this.rightRotate()
        break
      case AvlOperationArrangementType.LeftRight:
        subtree.leftRotate()
        this.rightRotate()
        break
      case AvlOperationArrangementType.RightLeft:
        subtree.rightRotate()
        this.leftRotate()
        break
      case AvlOperationArrangementType.RightRight:
        this.leftRotate()
        break
      default: {
        // https://www.typescriptlang.org/docs/handbook/2/narrowing.html#exhaustiveness-checking
        const exhaustiveCheck: never = arrangement
        throw exhaustiveCheck
      }
    }
  }
}
