-- https://github.com/dtaivpp/car_company_database
--
SELECT
  *
FROM
  models AS M,
  brands AS B
WHERE
  M.brand_id = B.id
ORDER BY
  M.name;

SELECT
  B.name,
  D.name,
  address
FROM
  dealers AS D,
  brands AS B,
  dealers_brands AS DB
WHERE
  DB.dealer_id = D.id
  AND DB.brand_id = B.id
ORDER BY
  1,
  2 ASC;

SELECT
  C.id,
  C.last_name,
  C.first_name,
  COW.purchase_date,
  COW.purchase_price,
  CV.id,
  M.name,
  B.name,
  D.name,
  MP.name,
  COP.color,
  E.name,
  T.name,
  CH.name
FROM
  customers AS C,
  customer_ownerships AS COW,
  car_vins AS CV,
  models AS M,
  brands AS B,
  dealers AS D,
  manufacture_plants AS MP,
  car_options AS COP,
  car_parts AS E,
  car_parts AS T,
  car_parts AS CH
WHERE
  COW.customer_id = C.id
  AND COW.vin_id = CV.id
  AND CV.model_id = M.id
  AND M.brand_id = B.id
  AND COW.dealer_id = D.id
  AND CV.manufacture_plant_id = MP.id
  AND CV.option_set_id = COP.id
  AND COP.engine_id = E.id
  AND COP.transmission_id = T.id
  AND COP.chassis_id = CH.id
ORDER BY
  4 DESC,
  2,
  3;