// deno repl --v8-flags=--stack-size=8192,--max-old-space-size=8192 --allow-read --allow-net
// import { printObject, log, printTable, tap } from './src/util.ts'; import { database, runQuery, printQuery } from './src/populate_db.ts'
// runQuery(await Deno.readTextFile('./sample_data/create_tables.sql'))
// runQuery(await Deno.readTextFile('./sample_data/inserts.sql'))
// printQuery(`
//   SELECT
//   *
//   FROM
//   models AS M,
//   brands AS B
//   WHERE
//   M.brand_id = B.id
//   ORDER BY
//   M.name;
// `)
