INSERT INTO
  dealers (name, address)
VALUES
  ('Joes Autos', '123 Abc Lane Virginia');

INSERT INTO
  dealers (name, address)
VALUES
  ('Priority X', '999 Nein-Nein Virginia');

INSERT INTO
  dealers (name, address)
VALUES
  ('Priority Y', '404 Address Not Found');

INSERT INTO
  dealers (name, address)
VALUES
  ('Priority Z', '8675309 Jessies Mom');

INSERT INTO
  brands(name)
VALUES
  ('Cover Squirrel');

INSERT INTO
  brands(name)
VALUES
  ('Supreme');

INSERT INTO
  brands(name)
VALUES
  ('Yellow');

INSERT INTO
  brands(name)
VALUES
  ('Ferrari');

INSERT INTO
  brands(name)
VALUES
  ('Boujiee');

INSERT INTO
  brands(name)
VALUES
  ('Freshest');

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (1, 1);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (1, 3);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (1, 6);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (1, 5);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (2, 2);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (2, 3);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (2, 5);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (2, 1);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (3, 5);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (3, 6);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (3, 2);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (4, 2);

INSERT INTO
  dealers_brands(dealer_id, brand_id)
VALUES
  (4, 4);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('The Blonde', 23000, 1);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('The Brunette', 25000, 1);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('The Red Head', 29000, 1);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Hat', 22000, 2);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Sweater', 25000, 2);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('T-Shirt', 27000, 2);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Orange', 15000, 3);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Blue', 12000, 3);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Green', 17000, 3);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('LaFerrari', 125000, 4);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('450', 75000, 4);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('F12 Berlinetta', 110000, 4);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('F40', 100000, 4);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Extra', 30000, 5);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Too Much', 35000, 5);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Beats', 24000, 6);

INSERT INTO
  models(name, base_price, brand_id)
VALUES
  ('Bars', 35000, 6);

INSERT INTO
  manufacture_plants(
    name,
    type,
    location,
    company_owned
  )
VALUES
  ('West Plant', 'Parts', 'Europe', true);

INSERT INTO
  manufacture_plants(
    name,
    type,
    location,
    company_owned
  )
VALUES
  ('East Plant', 'Parts', 'Asia', true);

INSERT INTO
  manufacture_plants(
    name,
    type,
    location,
    company_owned
  )
VALUES
  ('North Plant', 'Assembly', 'Asia', true);

INSERT INTO
  manufacture_plants(
    name,
    type,
    location,
    company_owned
  )
VALUES
  ('South Plant', 'Assembly', 'Australia', true);

INSERT INTO
  manufacture_plants(
    name,
    type,
    location,
    company_owned
  )
VALUES
  ('Scranton Branch', 'Parts', 'USA', false);

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  ('2.6L Engine', 1, '2012-08-12', '2013-08-12');

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  ('3.0L Engine', 2, '2012-07-10', '2017-02-20');

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  ('2.4L Engine', 5, '2012-08-12', '2013-08-12');

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  (
    'Jetrag 6 Speed',
    5,
    '2012-02-11',
    '2013-08-12'
  );

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  ('Bose Audio', 1, '2014-08-02', '2015-03-12');

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date
  )
VALUES
  ('4WD Chassis', 2, '2000-08-29', '2017-05-12');

INSERT INTO
  car_parts(
    name,
    manufacture_plant_id,
    manufacture_start_date,
    manufacture_end_date,
    part_recall
  )
VALUES
  (
    '2WD Chassis',
    2,
    '2005-12-20',
    '2017-11-01',
    true
  );

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (4, 2, 4, 6, 5, 'Blue', 2000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (5, 2, 4, 6, 5, 'Yellow', 1200);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (6, 1, 4, 7, 5, 'Green', 1100);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (15, 1, 4, 7, 5, 'Red', 4000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (14, 2, 4, 7, 5, 'Sky', 3000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (16, 2, 4, 7, 5, 'Birthday Cake', 2500);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (17, 3, 4, 6, 5, 'Cyan', 7000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (10, 1, 4, 6, 5, 'Purple', 2500);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (11, 2, 4, 6, 5, 'Red', 10000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (12, 2, 4, 7, 5, 'Blue', 11300);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    premium_sound_id,
    color,
    option_set_price
  )
VALUES
  (13, 1, 4, 7, 5, 'Green', 23000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (1, 1, 4, 7, 'Black', 1100);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (2, 1, 4, 7, 'Blue', 2000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (3, 1, 4, 6, 'Red', 1500);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (4, 3, 4, 6, 'Yellow', 4000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (5, 2, 4, 6, 'Black', 3600);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (7, 2, 4, 6, 'White', 2300);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (8, 2, 4, 7, 'Pretty', 2200);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (9, 3, 4, 7, 'Glitter BomB', 2900);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (14, 1, 4, 7, 'Nyan', 7999);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (12, 1, 4, 7, 'Ultraviolet', 25000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (11, 1, 4, 7, 'Transparent', 40000);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (6, 2, 4, 6, 'Green', 1200);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (2, 1, 4, 6, 'Purple', 3900);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (1, 2, 4, 7, 'Red', 900);

INSERT INTO
  car_options(
    model_id,
    engine_id,
    transmission_id,
    chassis_id,
    color,
    option_set_price
  )
VALUES
  (11, 2, 4, 6, 'Magenta', 200);

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Jeremy',
    'Jacobs',
    'Male',
    120000,
    '1990-12-12',
    9177554315,
    'Jeremy@Gmail.com'
  );

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Maria',
    'Swabota',
    'Female',
    60000,
    '1980-06-15',
    7577749387,
    'Maria@Gmail.com'
  );

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Jacob',
    'Wong',
    'Male',
    24000,
    '1992-02-20',
    2129990234,
    'WonTon@hotmail.com'
  );

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Pitbull',
    'Perez',
    'Male',
    1200000,
    '1985-12-01',
    7892341827,
    'Pitbull@ymail.com'
  );

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Minnie',
    'Mouse',
    'Female',
    200000,
    '1950-03-01',
    7542890987,
    'MinnieMe@gmail.com'
  );

INSERT INTO
  customers(
    first_name,
    last_name,
    gender,
    household_income,
    birthdate,
    phone_number,
    email
  )
VALUES
  (
    'Jessica',
    'Parker',
    'Female',
    120000,
    '1989-06-29',
    4245679000,
    'JennyFromThePark@mymail.com'
  );

INSERT INTO
  car_vins(
    model_id,
    option_set_id,
    manufacture_date,
    manufacture_plant_id
  )
VALUES
  (7, 17, '2017-02-20', 4);

INSERT INTO
  car_vins(
    model_id,
    option_set_id,
    manufacture_date,
    manufacture_plant_id
  )
VALUES
  (14, 5, '2016-09-19', 3);

INSERT INTO
  car_vins(
    model_id,
    option_set_id,
    manufacture_date,
    manufacture_plant_id
  )
VALUES
  (12, 10, '2014-11-14', 3);

INSERT INTO
  car_vins(
    model_id,
    option_set_id,
    manufacture_date,
    manufacture_plant_id
  )
VALUES
  (2, 13, '2013-03-04', 4);

INSERT INTO
  car_vins(
    model_id,
    option_set_id,
    manufacture_date,
    manufacture_plant_id
  )
VALUES
  (4, 1, '2013-06-22', 4);

INSERT INTO
  customer_ownerships(
    customer_id,
    vin_id,
    purchase_date,
    purchase_price,
    warranty_expire_date,
    dealer_id
  )
VALUES
  (6, 1, '2017-12-21', 17200, '2025-05-12', 2);

INSERT INTO
  customer_ownerships(
    customer_id,
    vin_id,
    purchase_date,
    purchase_price,
    warranty_expire_date,
    dealer_id
  )
VALUES
  (5, 2, '2016-10-14', 33000, '2024-04-19', 1);

INSERT INTO
  customer_ownerships(
    customer_id,
    vin_id,
    purchase_date,
    purchase_price,
    warranty_expire_date,
    dealer_id
  )
VALUES
  (2, 3, '2015-12-01', 121300, '2026-03-03', 4);

INSERT INTO
  customer_ownerships(
    customer_id,
    vin_id,
    purchase_date,
    purchase_price,
    warranty_expire_date,
    dealer_id
  )
VALUES
  (3, 4, '2017-08-09', 26000, '2090-02-14', 1);

INSERT INTO
  customer_ownerships(
    customer_id,
    vin_id,
    purchase_date,
    purchase_price,
    warranty_expire_date,
    dealer_id
  )
VALUES
  (1, 5, '2017-02-24', 24000, '2027-01-12', 3);