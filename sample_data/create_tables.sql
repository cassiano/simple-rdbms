CREATE TABLE customers(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  first_name VARCHAR NOT NULL,
  last_name VARCHAR NOT NULL,
  gender VARCHAR,
  household_income INTEGER,
  birthdate VARCHAR NOT NULL,
  phone_number INTEGER NOT NULL,
  email VARCHAR
);

CREATE TABLE car_vins(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  model_id INTEGER NOT NULL,
  option_set_id INTEGER NOT NULL,
  manufacture_date VARCHAR NOT NULL,
  manufacture_plant_id INTEGER NOT NULL,
  FOREIGN KEY (model_id) REFERENCES models(id),
  FOREIGN KEY (manufacture_plant_id) REFERENCES manufacture_plants(id),
  FOREIGN KEY (option_set_id) REFERENCES car_options(id)
);

CREATE INDEX car_vins_model_id_index ON car_vins (model_id);

CREATE INDEX car_vins_manufacture_plant_id_index ON car_vins (manufacture_plant_id);

CREATE INDEX car_vins_option_set_id_index ON car_vins (option_set_id);

CREATE TABLE car_options(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  model_id INTEGER NULL,
  engine_id INTEGER NOT NULL,
  transmission_id INTEGER NOT NULL,
  chassis_id INTEGER NOT NULL,
  premium_sound_id INTEGER,
  color VARCHAR NOT NULL,
  option_set_price INTEGER NOT NULL,
  FOREIGN KEY (model_id) REFERENCES models(id),
  FOREIGN KEY (engine_id) REFERENCES car_parts(id),
  FOREIGN KEY (premium_sound_id) REFERENCES car_parts(id),
  FOREIGN KEY (transmission_id) REFERENCES car_parts(id),
  FOREIGN KEY (chassis_id) REFERENCES car_parts(id)
);

CREATE INDEX car_options_engine_id_index ON car_options (engine_id);

CREATE INDEX car_options_premium_sound_id_index ON car_options (premium_sound_id);

CREATE INDEX car_options_model_id_index ON car_options (model_id);

CREATE INDEX car_options_transmission_id_index ON car_options (transmission_id);

CREATE INDEX car_options_chassis_id_index ON car_options (chassis_id);

CREATE TABLE car_parts(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL,
  manufacture_plant_id INTEGER NOT NULL,
  manufacture_start_date VARCHAR NOT NULL,
  manufacture_end_date VARCHAR,
  part_recall BOOLEAN DEFAULT false,
  FOREIGN KEY (manufacture_plant_id) REFERENCES manufacture_plants(id)
);

CREATE INDEX car_parts_manufacture_plant_id_index ON car_parts (manufacture_plant_id);

CREATE TABLE brands(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL
);

CREATE TABLE models(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL,
  base_price INTEGER NOT NULL,
  brand_id INTEGER NOT NULL,
  FOREIGN KEY (brand_id) REFERENCES brands(id)
);

CREATE INDEX models_brand_id_index ON models (brand_id);

CREATE TABLE customer_ownerships(
  customer_id INTEGER NOT NULL,
  vin_id INTEGER NOT NULL,
  purchase_date VARCHAR NOT NULL,
  purchase_price INTEGER NOT NULL,
  warranty_expire_date VARCHAR,
  dealer_id INTEGER NOT NULL,
  PRIMARY KEY (customer_id, vin_id),
  FOREIGN KEY (customer_id) REFERENCES customers(id),
  FOREIGN KEY (vin_id) REFERENCES car_vins(id),
  FOREIGN KEY (dealer_id) REFERENCES dealers(id)
);

CREATE INDEX customer_ownerships_customer_id_index ON customer_ownerships (customer_id);

CREATE INDEX customer_ownerships_vin_index ON customer_ownerships (vin_id);

CREATE INDEX customer_ownerships_dealer_id_index ON customer_ownerships (dealer_id);

CREATE TABLE manufacture_plants(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL,
  type VARCHAR,
  location VARCHAR,
  company_owned BOOLEAN
);

CREATE TABLE dealers (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name VARCHAR NOT NULL,
  address VARCHAR
);

CREATE TABLE dealers_brands(
  dealer_id INTEGER NOT NULL,
  brand_id INTEGER NOT NULL,
  PRIMARY KEY (dealer_id, brand_id),
  FOREIGN KEY (dealer_id) REFERENCES dealers(id),
  FOREIGN KEY (brand_id) REFERENCES brands(id)
);

CREATE INDEX dealers_brands_brand_id_index ON dealers_brands (brand_id);